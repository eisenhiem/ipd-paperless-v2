<?php

use app\models\Labresult;
use app\models\Lbbk;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ipt */

$this->title = 'AN: '.$model->an;
\yii\web\YiiAsset::register($this);
?>
<div class="ipt-view">
  <div class="row">
    <div class="col-10 offset-2">
    <?= Html::a(Icon::show('chevron-left').'กลับ', ['index','ward_id' => $ward->ward], ['class' => 'btn btn-danger']) ?>
    <?= $ward->dchdate == '0000-00-00' ? Html::a(Icon::show('plus').'เพิ่ม Order', ['order/create','an'=>$model->an], ['class' => 'btn btn-success']):'' ?>
    <?= Html::a(Icon::show('clipboard').'Doctor Order', ['order/list','id'=>$model->an], ['class' => 'btn btn-info']) ?>
    <?= Html::a(Icon::show('notes-medical').'Nurse Note', ['note/list','id'=>$model->an], ['class' => 'btn btn-warning']) ?>
    </div>
  </div>
  <br>
<div class="card text-white bg-primary">
  <div class="card-header">ข้อมูลทั่วไป</div>
  <div class="card-body bg-white">
    <p class="card-text">
      &emsp;<b>HN: <?php echo $model->hn; ?></b>
      &emsp;<b>AN: <?php echo $model->an; ?></b>
      &emsp;<b>ชื่อ-สกุล: </b><?php echo $model->prename . $model->ptname; ?>
      &emsp;<b>อายุ </b><?php echo $model->age; ?> ปี
      &emsp;<b>เพศ </b><?php echo $ptinfo->sex; ?>
    </p>
  </div>
</div>
<br>
<div class="card text-white bg-warning">
  <div class="card-header">ผลการตรวจทางห้องปฏิบัติการ</div>
  <div class="card-body bg-white">
    <p class="card-text">
      <?php 
        $lab = Lbbk::find()->where(['vn' => $visit->vn])->groupBy('labcode')->all();
        foreach($lab as $l){
        ?>
        <div id="accordion">
          <div class="card">
            <div class="card-header bg-primary text-left" id="heading<?= $l->labcode ?>">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse<?= $l->labcode ?>" aria-expanded="false" aria-controls="collapse<?= $l->labcode ?>" style="color:white;">
                    <?= Html::a(Icon::show('print'), ['print-lab','vn'=>$l->vn,'labcode' => $l->labcode], ['class' => 'btn btn-light','target'=>'_blank']) ?>
                    &emsp;Lab : <?= $l->lab->labname ?> <?= $l->finish == '0' ? '<span class="right badge badge-light">ยังไม่เสร็จ</span>':'' ?> 
                    </button>
                </h5>
            </div>
            <div id="collapse<?= $l->labcode ?>" class="collapse" aria-labelledby="heading<?= $l->labcode ?>" data-parent="#accordion">
                <div class="card-body">
                    <ul>
                    <?php 
                    $labno = Lbbk::find()->where(['vn' => $visit->vn,'labcode' => $l->labcode])->orderBy(['ln'=> SORT_DESC])->all();
                    foreach($labno as $r){
                      $result = Labresult::find()->joinWith('label')->where(['ln'=>$r->ln])->orderBy(['id' => SORT_DESC])->all();
                      echo '<div class="btn btn-info btn-block">วันที่'.$r->getServdate().'</div>' ;
                      echo '<div class="row">';
                      foreach($result as $v){
                        echo '<div class="col-sm-6">'.$v->getLabelName().': <b>'.$v->labresult.'</b> '.$v->unit.' ['.$v->normal.']'.'</div>';
                      }
                      echo '</div>';
                    }
                    ?>
                    </ul>
                </div>
            </div>
        </div>
        </div>
        <?php         
        }
      ?>

    </p>
  </div>
</div>
</div>
