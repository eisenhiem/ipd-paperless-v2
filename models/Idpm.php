<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "idpm".
 *
 * @property string|null $nameidpm
 * @property string $costcenter
 * @property string $idpm
 * @property string $dspname
 * @property string $costcente2
 * @property string $op56
 * @property int|null $bed
 * @property int $bed_std
 * @property string $wardlis
 * @property string|null $liscode
 */
class Idpm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'idpm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['costcenter', 'idpm', 'dspname', 'costcente2', 'op56', 'bed_std', 'wardlis'], 'required'],
            [['bed', 'bed_std'], 'integer'],
            [['nameidpm', 'dspname'], 'string', 'max' => 30],
            [['costcenter', 'costcente2'], 'string', 'max' => 8],
            [['idpm'], 'string', 'max' => 2],
            [['op56', 'liscode'], 'string', 'max' => 5],
            [['wardlis'], 'string', 'max' => 10],
            [['idpm'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nameidpm' => 'Nameidpm',
            'costcenter' => 'Costcenter',
            'idpm' => 'Idpm',
            'dspname' => 'Dspname',
            'costcente2' => 'Costcente 2',
            'op56' => 'Op 56',
            'bed' => 'Bed',
            'bed_std' => 'Bed Std',
            'wardlis' => 'Wardlis',
            'liscode' => 'Liscode',
        ];
    }
}
