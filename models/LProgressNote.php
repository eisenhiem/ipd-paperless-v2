<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "l_progress_note".
 *
 * @property int $progress_id
 * @property string $specialist_group_code
 * @property string $progress_type
 * @property string $progress_value
 * @property string|null $d_update
 * @property string|null $is_active
 */
class LProgressNote extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'l_progress_note';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['specialist_group_code', 'progress_type', 'progress_value'], 'required'],
            [['progress_type'], 'string'],
            [['d_update', 'is_active'], 'safe'],
            [['specialist_group_code'], 'string', 'max' => 10],
            [['progress_value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'progress_id' => 'Progress ID',
            'specialist_group_code' => 'แพทย์เฉพาะทางสาขา',
            'progress_type' => 'ประเภท',
            'progress_value' => 'Progress Note',
            'd_update' => 'D Update',
        ];
    }
}
