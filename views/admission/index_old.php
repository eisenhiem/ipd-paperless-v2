<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ipts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ipt-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ipt', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'vn',
            'an',
            'hn',
            'ward',
            'rgtdate',
            //'rgttime:datetime',
            //'pttype',
            //'prediag',
            //'dchdate',
            //'daycnt',
            //'dchtime:datetime',
            //'dchstts',
            //'dchtype',
            //'dthdiagdct',
            //'ipdlct',
            //'bmi',
            //'bw',
            //'height',
            //'tt',
            //'rr',
            //'pr',
            //'sbp',
            //'dbp',
            //'ipttype',
            //'painscore',
            //'symptmi',
            //'drg',
            //'rw',
            //'adjrw',
            //'error',
            //'warning',
            //'actlos',
            //'grouper_version',
            //'wtlos',
            //'ot',
            //'dept',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action,$model, $key, $index, $column) {
                    return Url::toRoute([$action, 'an' => $model->an]);
                 }
            ],
        ],
    ]); ?>


</div>
