<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DoctorOrders */

$this->title = $model->order_id;
\yii\web\YiiAsset::register($this);
?>
<div class="doctor-orders-view">

    <p>
        <?= Html::a('Update', ['update', 'order_id' => $model->order_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'order_id' => $model->order_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'order_id',
            'an',
            'order_datetime',
            'progress_note:ntext',
            'order_oneday:ntext',
            'order_continue:ntext',
            'order_comment:ntext',
            'user_id',
            'd_update',
        ],
    ]) ?>

</div>
