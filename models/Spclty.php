<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "spclty".
 *
 * @property string $namespclty
 * @property string $spclty
 */
class Spclty extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'spclty';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['namespclty', 'spclty'], 'required'],
            [['namespclty'], 'string', 'max' => 40],
            [['spclty'], 'string', 'max' => 2],
            [['spclty'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'namespclty' => 'Namespclty',
            'spclty' => 'Spclty',
        ];
    }
}
