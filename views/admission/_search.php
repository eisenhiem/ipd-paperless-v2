<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IptSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ipt-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index','ward_id' => $id],
        'method' => 'get',
    ]); ?>
    <div class="row">
    <div class="col-md-3 col-sm-4 col-xs-5">
    <?= $form->field($model, 'an')->textInput(['placeholder' => 'ระบุ AN'])->label(false); ?>
    </div>
    <div class="col-md-3 col-sm-4 col-xs-5">
    <?= $form->field($model, 'hn')->textInput(['placeholder' => 'ระบุ HN'])->label(false); ?>
    </div>
    <div class="col-2">
    <div class="form-group">
        <?= Html::submitButton(Icon::show('search').' ค้นหา', ['class' => 'btn btn-primary']) ?>
    </div>        
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
