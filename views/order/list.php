<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Admission;
use app\models\Ipt;
use app\models\Labresult;
use app\models\Lbbk;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AdmissionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Order ชื่อ-สกุล:' . $ward->pt->getFullName();
?>
<div class="admission-index">

    <p>
        <?= Html::a(Icon::show('chevron-left') . 'กลับ', ['admission/index', 'ward_id' => $ward->ward], ['class' => 'btn btn-danger']) ?>
        <?= $ward->dchdate == '0000-00-00' && in_array(Yii::$app->user->identity->role ,[5,8,9]) ? Html::a(Icon::show('plus') . 'เพิ่ม Order', ['create', 'an' => $an], ['class' => 'btn btn-success']) : '' ?>
        <?php //echo  $order->value1 == 'asc' ? Html::a(Icon::show('sort-amount-down').'เรียง Z > A', ['switchsort','id' => $an], ['class' => 'btn btn-primary']):Html::a(Icon::show('sort-amount-down-alt').'เรียง A > Z', ['switchsort','id' => $an], ['class' => 'btn btn-primary']); 
        ?>
        <?= Html::a(Icon::show('flask') . 'Lab', ['admission/lab', 'id' => $an], ['class' => 'btn btn-warning']) ?>
        <?= Html::a(Icon::show('thermometer-half') . 'V/S', ['note/vitalsign', 'id' => $an], ['class' => 'btn btn-info']) ?>
        <?= Html::a(Icon::show('notes-medical') . 'Nurse Note ', ['note/list', 'id' => $an], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Icon::show('print') . ' พิมพ์ Doctor Order', ['print', 'id' => $an], ['class' => 'btn btn-warning', 'target' => '_blank']) ?>
    </p>

    <table width="100%" border="1">
        <tr>
            <th width="20%" style="text-align:center">Progress Note</th>
            <th width="35%" style="text-align:center">Order One Day</th>
            <th width="35%" style="text-align:center">Order Continue</th>
            <th width="10%" style="text-align:center">หมายเหตุ</th>
        </tr>
        <?php
        $i = 1;
        if ($model) {
            foreach ($model as $order) {
        ?>
                <tr>
                    <td style="vertical-align:top;padding-left: 10px;">
                        <?= Ipt::getThaiDateTime($order->order_datetime) ?><br><br>
                        <?= $order->specialist_progress_note ? nl2br($order->specialist_progress_note) : '' ?>
                        <?= $order->progress_note ? nl2br($order->progress_note) : '' ?><br><br>
                        <b>Order By :</b>
                        <?= $order->profile->fullname ?><br>
                        <?= $order->profile->position ?><br>
                        <?= $order->profile->license_no ?><br>
                        <?= !$order->receive_id ? Html::a('รับ Order', ['receive', 'order_id' => $order->order_id], ['class' => 'btn btn-success', 'style' => 'width:200px']) : '<b>รับ Order : </b>'.$order->receive->fullname.'<br>'.$order->receive->position ?>
                    </td>
                    <td style="vertical-align:top;padding-left: 10px;padding-right: 5px;">
                        <?= $order->order_oneday ? nl2br($order->order_oneday) : '' ?><br>
                        <?= $order->labs ? '<b>Investigation: </b>'.nl2br($order->labs) : '' ?>
                        <?= $order->xrays ? ','.nl2br($order->xrays) : '' ?>
                    </td>
                    <td style="vertical-align:top;padding-left: 10px;">
                        <?= $order->food ? $order->getFoodName() . '<br>' : '' ?>
                        <?= $order->record_io ? '&emsp; [ / ] Record I/O <br>' : '' ?>
                        <?= $order->record_vitalsign ? '&emsp; [ / ] Record V/S <br>' : '' ?>
                        <?php
                        if ($continue && $i == 1) {
                            foreach ($continue as $c) {
                                echo Ipt::getThaiDateTime($c->order_datetime) . '<br>';
                                echo '&emsp;' . nl2br($c->order_continue) . '<br>';
                            }
                        } ?>
                        <?= $order->order_continue && $i != 1 ? '<br>' . nl2br($order->order_continue) : '' ?>
                    </td>
                    <td style="vertical-align:top;padding-left: 5px;;padding-right: 5px;">
                        <?= ((Yii::$app->user->identity->id == $order->user_id) && ($ward->dchdate == '0000-00-00') && $i == 1 && ($order->order_status == 'Order')) ? Html::a(Icon::show('edit') . ' แก้ไข', ['update', 'order_id' => $order->order_id], ['class' => 'btn btn-warning btn-block']) : '' ?>
                        <?= Html::a(Icon::show('print') . ' พิมพ์', ['order', 'id' => $order->order_id], ['class' => 'btn btn-info btn-block', 'target' => '_blank']) ?><br>
                        <?= (($ward->dchdate == '0000-00-00') && $i == 1) ? Html::a(Icon::show('plus') . ' Consult', ['consult/create', 'id' => $order->order_id], ['class' => 'btn btn-success btn-block']) : '' ?><br>
                        <?= (($order->order_status == 'Order') && $i == 1) ? Html::a(Icon::show('reply') . ' รับ Order', ['receive', 'order_id' => $order->order_id], ['class' => 'btn btn-primary btn-block']) : '' ?><br>
                        <?= nl2br($order->order_comment) ?>
                        <br>
                        <?= $order->receive_id ? '<b>รับ Order By:</b>':'' ?>
                        <?= $order->receive->fullname ?><br>
                        <?= $order->receive->position ?><br>
                    </td>
                </tr>
        <?php
                $i++;
            }
        }
        ?>
    </table>
    <br>
    <div style="text-align:center">
        <?= !$ward->dchdate == '0000-00-00' ? Html::a('Discharge คนไข้', ['admission/dc', 'id' => $an], ['class' => 'btn btn-danger', 'style' => 'width:200px']) : '' ?>
    </div>
</div>