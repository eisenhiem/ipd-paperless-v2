<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LProgressNote */

$this->title = 'Update L Progress Note: ' . $model->progress_id;
$this->params['breadcrumbs'][] = ['label' => 'L Progress Notes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->progress_id, 'url' => ['view', 'progress_id' => $model->progress_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lprogress-note-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
