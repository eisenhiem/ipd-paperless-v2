<?php

use app\models\DoctorOrders;
use app\models\Idpm;
use app\models\Ipt;
use yii\helpers\Html;

$ward = Idpm::find()->all();
$total = Idpm::find()->count();
$admit = Ipt::find()->where(['rgtdate'=>date('y-m-d')])->count();
$pt = Ipt::find()->where(['rgtdate'=>date('y-m-d')])->andWhere(['ward' => ''])->count();
$admit_yesterday = Ipt::find()->where(['rgtdate'=>date('Y-m-d', strtotime("-1 day"))])->count();
$dc = Ipt::find()->where(['dchdate'=>date('Y-m-d')])->count();
$dc_yesterday = Ipt::find()->where(['dchdate'=>date('Y-m-d', strtotime("-1 day"))])->count();

$color = [
  'background:#FDE1B8;',
  'background:#ADE288;',
  'background:#DEF197;',
  'background:#F8F4D8;',
  'background:#BBD6EC;',
  'background:#CAEBED;',
  'background:#BCE5E2;',
  'background:#F7A8B2;',
  'background:#F6D7C5;',
  'background:#D8C3E0;',
  'background:#D1EED3;',
  'background:#B3DFB5;',
  'background:#F9F4CF;',
  'background:#BDD0EB;',
  'background:#F8E3D4;',
  'background:#D6D0F5;',
  'background:#F5D4E9;',
  'background:#D8C9FF;',
  'background:#F3D5FB;',
  'background:#D6FFC7;',
  'background:#FBFFD4;',
  'background:#D1F5F5;',
  'background:#C7E9FF;',
  'background:#FFF0F9;',
  'background:#FFDDF4;',
  'background:#D8D0F2;',
  'background:#FCEECF;',
  'background:#BAE3E6;',
  'background:#AAC6E6;',
  'background:#AEA0DB;',
  'background:#EFB5E5;',
];
/* @var $this yii\web\View */
$this->title = 'IPD Paperless';
?>
<div class="site-index">
<div class="body-content">
<div class="col-md-4 offset-md-4"> 
<?php if(Yii::$app->user->identity->role == 4 ){
  echo Html::a('ยังไม่ระบุ'.' <span class="right badge badge-danger">'.$pt.'</span>',['order/list-order','ward_id'=>'' ] , ['class' => 'btn btn-lg btn-block','style'=> 'margin-bottom: 20px;background:#EFB5E5; color:black']); 
} else {
  echo Html::a('ยังไม่ระบุ'.' <span class="right badge badge-danger">'.$pt.'</span>',['admission/index','ward_id'=>'' ] , ['class' => 'btn btn-lg btn-block','style'=> 'margin-bottom: 20px;background:#EFB5E5; color:black']); 
}

?>
</div>
<?php 
$i=0;
    foreach($ward as $w){
      $bed = 0;
      $bed = Ipt::find()->where(['ward'=>$w->idpm,'dchdate'=>'0000-00-00'])->count();
      $new = Ipt::find()->where(['ward'=>$w->idpm,'rgtdate'=> date('Y-m-d')])->count();
      $order = DoctorOrders::find()->where(['<>','order_status','Complete'])->andWhere(['ward'=>$w->idpm,])->count();
      if($i%3==0){
        echo '<div class="row">';
      }
      if(Yii::$app->user->identity->role == 4 ){
        if($order == 0){
          echo '<div class="col-md-4">'.Html::a($w->nameidpm ,['order/list-order','ward_id'=>$w->idpm] , ['class' => 'btn btn-lg btn-block','style'=> 'margin-bottom: 20px;'.$color[$i].'color:black']).'</div>';
        } else {
          echo '<div class="col-md-4">'.Html::a($w->nameidpm.' <span class="right badge badge-danger">'.$order.'</span>',['order/list-order','ward_id'=>$w->idpm] , ['class' => 'btn btn-lg btn-block','style'=> 'margin-bottom: 20px;'.$color[$i].'color:black']).'</div>';
        }
      } else {
        if($bed == 0){
          echo '<div class="col-md-4">'.Html::a($w->nameidpm ,['admission/index','ward_id'=>$w->idpm] , ['class' => 'btn btn-lg btn-block','style'=> 'margin-bottom: 20px;'.$color[$i].'color:black']).'</div>';
        } else {
          echo '<div class="col-md-4">'.Html::a($w->nameidpm.' <span class="right badge badge-warning">'.$bed.'</span>',['admission/index','ward_id'=>$w->idpm] , ['class' => 'btn btn-lg btn-block','style'=> 'margin-bottom: 20px;'.$color[$i].'color:black']).'</div>';
        }
      }
      $i=$i+1; 
      if($i%3==0){
        echo '</div>';
      }
      if($i == $total){
        echo '</div>';
      }
    }
?>
</div>
</div>