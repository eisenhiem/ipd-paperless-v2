<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LActivity */

$this->title = 'Create L Activity';
?>
<div class="lactivity-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
