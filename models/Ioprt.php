<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ioprt".
 *
 * @property int $an
 * @property int $hn
 * @property string $date
 * @property int $time
 * @property string|null $dct
 * @property int $orno
 * @property string $icd9cm
 * @property string $icd9name
 * @property string $optype
 * @property int $charge
 * @property int $rcptno
 * @property string|null $oppnote
 * @property string $codeicd9id
 * @property int|null $qty
 * @property int $id
 */
class Ioprt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ioprt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'hn', 'time', 'orno', 'icd9cm', 'icd9name', 'optype', 'charge', 'rcptno', 'codeicd9id'], 'required'],
            [['an', 'hn', 'time', 'orno', 'charge', 'rcptno', 'qty'], 'integer'],
            [['date'], 'safe'],
            [['oppnote'], 'string'],
            [['dct'], 'string', 'max' => 5],
            [['icd9cm', 'codeicd9id'], 'string', 'max' => 7],
            [['icd9name'], 'string', 'max' => 90],
            [['optype'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'An',
            'hn' => 'Hn',
            'date' => 'Date',
            'time' => 'Time',
            'dct' => 'Dct',
            'orno' => 'Orno',
            'icd9cm' => 'Icd 9cm',
            'icd9name' => 'Icd 9name',
            'optype' => 'Optype',
            'charge' => 'Charge',
            'rcptno' => 'Rcptno',
            'oppnote' => 'Oppnote',
            'codeicd9id' => 'Codeicd 9id',
            'qty' => 'Qty',
            'id' => 'ID',
        ];
    }
}
