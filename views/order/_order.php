<?php

use yii\helpers\Html;
use app\models\Admission;
use app\models\Ipt;

?>
<h3 align="center"><?= $office->value2 ?><br><?= $office->value3 ?> <?= $office->values4 ?>
  <br>Doctor Orders Sheet & Progress Note
</h3>
<table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
  <tr>
    <td width="35%" style="padding-left:5px;padding-right:5px;"><b>ชื่อ-สกุล </b><br>&emsp; &emsp; &emsp;<?= $pt->pt->getFullName() ?></td>
    <td width="25%"><b>&emsp;Ward </b> <?= $order->getWardname() ?><br>&emsp; <b>Bed</b> <?= $order->bed_no ?></td>
    <td width="20%">&emsp; <b>อายุ</b> <?= $pt->pt->getAge() ?> ปี<br>&emsp; <b>เพศ </b> <?= $pt->pt->getGender() ?></td>
    <td width="20%">&emsp; <b>HN:</b> <?= $pt->hn ?><br>&emsp; <b>AN:</b> <?= $pt->an ?></td>
  </tr>
</table>
<table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
  <tr>
    <th width="24%">Progress Note</th>
    <th width="28%">Order One Day</th>
    <th width="28%">Order Continue</th>
    <th width="20%">หมายเหตุ</th>
  </tr>
  <tr>
    <td height=700 valign=top style="padding-left:5px;padding-right:5px;">
      <?= Ipt::getThaiDateTime($order->order_datetime) ?><br>
      <?= $order->specialist_progress_note ? $order->getProgName() . '<br>' : '' ?>
      <?= $order->progress_note ?><br>
      <b>Order By :</b><br>
      <?= $order->profile->fullname ?><br>
      <?= $order->profile->position ?><br>
      <?= $order->profile->license_no ?><br>
      <?= $order->receive_id ? '<b>รับ Order : </b>'.$order->receive->fullname.'<br>'.$order->receive->position:'' ?>

    <td valign=top style="padding-left:5px;padding-right:5px;">
      <?= $order->labs ? '<b><u>Lab</u> ::</b><br>' . $order->getLabsName() . '<br>' : '' ?>
      <?= $order->xrays ? '<b><u>X-Ray</u> ::</b><br>' . $order->getXraysName() . '<br>' : '' ?>
      <?= $order->order_oneday ?>
    <td valign=top style="padding-left:5px;padding-right:5px;">
      <?= $order->food ? $order->getFoodName() : '' ?>
      <?= $order->extra_food ? ' ระบุ ' . $order->extra_food . '<br>' : '<br>' ?>
      <?= $order->record_io ? '&emsp; [ / ] Record I/O <br>' : '' ?>
      <?= $order->record_vitalsign ? '&emsp; [ / ] Record V/S <br>' : '' ?>
      <?= $order->order_continue ? nl2br($order->order_continue) : '' ?>
      <?php /*
      foreach ($order->getContinueOrders() as $c) {
        echo Ipt::getThaiDateTime($c->order_datetime) . '<br>';
        echo nl2br($c->order_continue) . '<br>';
      } */
      ?>
    </td>
    <td style="padding-left:5px;padding-right:5px;vertical-align:top;">
      <?= $order->order_comment ?>
      <br><br>
      <?= $order->receive_id ? '<b>รับ Order By:</b>' : '' ?><br>
      <?= $order->receive->fullname ?><br>
      <?= $order->receive->position ?><br>
    </td>
  </tr>
</table>
<hr>