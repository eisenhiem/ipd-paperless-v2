<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\NurseNotes */

$this->title = $model->note_id;
\yii\web\YiiAsset::register($this);
?>
<div class="nurse-notes-view">

    <p>
        <?= Html::a('Update', ['update', 'note_id' => $model->note_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'note_id' => $model->note_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'note_id',
            'note_date',
            'note_time',
            'activity:ntext',
            'evaluate:ntext',
            'user_id',
            'd_update',
        ],
    ]) ?>

</div>
