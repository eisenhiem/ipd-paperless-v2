<?php

namespace app\controllers;

use app\models\HiSetup;
use app\models\Ipt;
use app\models\NurseNotes;
use app\models\NurseNotesSearch;
use app\models\Tpr;
use kartik\mpdf\Pdf;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

date_default_timezone_set('Asia/Bangkok');

/**
 * NoteController implements the CRUD actions for NurseNotes model.
 */
class NoteController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ]
                    ]
                ]
            ]
        );
    }

    /**
     * Lists all NurseNotes models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new NurseNotesSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

            /**
     * Lists all Orders where AN .
     * @return mixed
     */
    public function actionList($id)
    {
        $order = HiSetup::find()->where(['common_code'=>'order_sort'])->one();
        if($order->value1 =='desc'){
            $model = NurseNotes::find()->where(['an'=>$id])->orderBy(['note_id' => SORT_DESC])->all();
        } else {
            $model = NurseNotes::find()->where(['an'=>$id])->orderBy(['note_id' => SORT_ASC])->all();
        }
        $ward = Ipt::find()->where(['an'=>$id])->one();
        $an = $id;
        return $this->render('list', [
            'model' => $model,
            'an' => $an,
            'ward' =>$ward,
            'order' => $order,
        ]);
    }

    /**
     * Displays a single NurseNotes model.
     * @param int $note_id Note ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($note_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($note_id),
        ]);
    }

    /**
     * Creates a new NurseNotes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($an)
    {
        $model = new NurseNotes();
        $admit = Ipt::find()->where(['an'=>$an])->one();

        $total = NurseNotes::find()->where(['an'=>$an])->count();
        $model->an = $an;
        $model->user_id = Yii::$app->user->identity->id;
        $model->note_date = date('Y-m-d');
        $model->note_time = date('H:i');

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['list', 'id' => $model->an]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'admit' => $admit,
            'total' => $total
        ]);
    }

    public function actionAddVs($an)
    {
        $model = new Tpr();
        $admit = Ipt::find()->where(['an'=>$an])->one();

        $model->an = $an;
        $model->user_id = Yii::$app->user->identity->id;
        $model->dttm = date('Y-m-d H:i:s');
        $model->record_time = date('H:i');
        $model->oral_fluid = 0;
        $model->parenteral = 0;
        $model->urine_fluid = 0;
        $model->emesis = 0;
        $model->drainage = 0;
        $model->aspiration = 0;

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                
                $model->intake = $model->oral_fluid + $model->parenteral;
                $model->output = $model->urine_fluid + $model->emesis + $model->drainage + $model->aspiration;
                if($model->save()){
                    return $this->redirect(['list', 'id' => $model->an]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('add-vs', [
            'model' => $model,
            'admit' => $admit,
        ]);
    }

    /**
     * Updates an existing NurseNotes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $note_id Note ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($note_id)
    {
        $model = $this->findModel($note_id);
        $model->actToArray();
        $model->evaToArray();

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['list', 'id' => $model->an]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NurseNotes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $note_id Note ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($note_id)
    {
        $this->findModel($note_id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Lists all Orders where AN .
     * @return mixed
     */
    public function actionPrint($id)
    {
        $model = NurseNotes::find()->where(['an'=>$id])->all();
        $pt = Ipt::find()->where(['an'=>$id])->one();
        $total = NurseNotes::find()->where(['an'=>$id])->count();
        $office = HiSetup::find()->where(['common_code' => 'hosp_info'])->one();

        $content = $this->renderPartial('_print',[
            'model' => $model,
            'pt' => $pt,
            'total' => $total,
            'office' => $office,
        ]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Nurse Note : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    public function actionVitalsign($id)
    {
        $bt = [];
        $pr = [];
        $rr = [];
        $sbp = [];
        $dbp = [];
        $intake = [];
        $outtake = [];
        $date = [];
        $urine = [];
        $stool = [];
        $an=$id;
        $model = Tpr::find()->where(['an'=>$id])->all();
        foreach($model as $vs){
            if($vs->tt){
                array_push($bt,intVal($vs->tt));
            } else {
                array_push($bt,0);
            }
            if($vs->sbp){
                array_push($sbp,intVal($vs->sbp));
            } else {
                array_push($sbp,0);
            }
            if($vs->dbp){
                array_push($dbp,intVal($vs->dbp));
            } else {
                array_push($dbp,0);
            }
            if($vs->pr){
                array_push($pr,intVal($vs->pr));
            } else {
                array_push($pr,0);
            }
            if($vs->rr){
                array_push($rr,intVal($vs->rr));
            } else {
                array_push($rr,0);
            }
            if($vs->intake){
                array_push($intake,intVal($vs->intake));
            } else {
                array_push($intake,0);
            }
            if($vs->output){
                array_push($outtake,intVal($vs->output));
            } else {
                array_push($outtake,0);
            }
            if($vs->s){
                array_push($stool,intVal($vs->s));
            } else {
                array_push($stool,0);
            }
            if($vs->u){
                array_push($urine,intVal($vs->u));
            } else {
                array_push($urine,0);
            }
            array_push($date,substr($vs->dttm,0,10).' '.$vs->record_time);
        }

        return $this->render('vitalsign',[
            'bt' => $bt,
            'sbp' => $sbp,
            'dbp' => $dbp,
            'pr' => $pr,
            'rr' => $rr,
            'intake' => $intake,
            'outtake' => $outtake,
            'stool' => $stool,
            'urine' => $urine,
            'date' => $date,
            'an' => $an,
        ]);
    }

    /**
     * Lists all Orders where AN .
     * @return mixed
     */
    public function actionPrintVitalsign($id)
    {
        $bt = [];
        $pr = [];
        $rr = [];
        $sbp = [];
        $dbp = [];
        $intake = [];
        $outtake = [];
        $date = [];
        $urine = [];
        $stool = [];
        $an=$id;
        $model = Tpr::find()->where(['an'=>$id])->all();
        foreach($model as $vs){
            if($vs->tt){
                array_push($bt,intVal($vs->tt));
            } else {
                array_push($bt,0);
            }
            if($vs->sbp){
                array_push($sbp,intVal($vs->sbp));
            } else {
                array_push($sbp,0);
            }
            if($vs->dbp){
                array_push($dbp,intVal($vs->dbp));
            } else {
                array_push($dbp,0);
            }
            if($vs->pr){
                array_push($pr,intVal($vs->pr));
            } else {
                array_push($pr,0);
            }
            if($vs->rr){
                array_push($rr,intVal($vs->rr));
            } else {
                array_push($rr,0);
            }
            if($vs->intake){
                array_push($intake,intVal($vs->intake));
            } else {
                array_push($intake,0);
            }
            if($vs->output){
                array_push($outtake,intVal($vs->output));
            } else {
                array_push($outtake,0);
            }
            if($vs->s){
                array_push($stool,intVal($vs->s));
            } else {
                array_push($stool,0);
            }
            if($vs->u){
                array_push($urine,intVal($vs->u));
            } else {
                array_push($urine,0);
            }
            array_push($date,substr($vs->dttm,0,10).' '.$vs->timetest);
        }

        $content = $this->renderPartial('_vitalsign',[
            'bt' => $bt,
            'sbp' => $sbp,
            'dbp' => $dbp,
            'pr' => $pr,
            'rr' => $rr,
            'intake' => $intake,
            'outtake' => $outtake,
            'stool' => $stool,
            'urine' => $urine,
            'date' => $date,
            'an' => $an,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Vital Sign : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    /**
     * Finds the NurseNotes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $note_id Note ID
     * @return NurseNotes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($note_id)
    {
        if (($model = NurseNotes::findOne(['note_id' => $note_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
