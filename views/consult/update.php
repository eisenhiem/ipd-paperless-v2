<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IptConsult */

$this->title = 'Update Ipt Consult: ' . $model->consult_id;
$this->params['breadcrumbs'][] = ['label' => 'Ipt Consults', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->consult_id, 'url' => ['view', 'consult_id' => $model->consult_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ipt-consult-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
