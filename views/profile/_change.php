<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$role = [1=>'ผู้ช่วยเหลือคนไข้',2=>'เวชปฏิบัติฯ',3=>'พยาบาล', 4=>'เภสัชกร' , 5 =>'แพทย์ ',6 =>'กายภาพ', 7=>'ทันตะ', 8=>'แพทย์เฉพาะทาง' ,9=>'Admin'];
/* @var $this yii\web\View */
/* @var $model app\models\Profile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profile-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-8 offset-2">
    <div class="card text-right">
        <div class="card-body">
            <div class="col-md-8 offset-2 col-sm-10 offset-1">

                <div class="row" style="vertical-align: middle;">
                    <div class="col-md-3">
                        สิทธิ์การเข้าถึง :
                    </div>
                    <div class="col-md-9">
                    <?= $form->field($model, 'role')->dropDownList($role)->label(false) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="form-group">
                <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success btn-block']) ?>
            </div>
        </div>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>