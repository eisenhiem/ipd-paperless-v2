<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LStandingOrder;

/**
 * LStandingOrderSearch represents the model behind the search form of `app\models\LStandingOrder`.
 */
class LStandingOrderSearch extends LStandingOrder
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['standing_order_id', 'user_id', 'qty'], 'integer'],
            [['standing_order_name', 'standing_order_type', 'standing_order_map_code', 'medusage', 'is_active'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LStandingOrder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'standing_order_id' => $this->standing_order_id,
            'user_id' => $this->user_id,
            'qty' => $this->qty,
        ]);

        $query->andFilterWhere(['like', 'standing_order_name', $this->standing_order_name])
            ->andFilterWhere(['like', 'standing_order_type', $this->standing_order_type])
            ->andFilterWhere(['like', 'standing_order_map_code', $this->standing_order_map_code])
            ->andFilterWhere(['like', 'medusage', $this->medusage])
            ->andFilterWhere(['like', 'is_active', $this->is_active]);

        return $dataProvider;
    }
}
