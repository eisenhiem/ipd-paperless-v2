<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "doctor_orders".
 *
 * @property int $order_id
 * @property int $an
 * @property int|null $ward
 * @property string $order_datetime
 * @property string|null $progress_note
 * @property string|null $order_oneday
 * @property string|null $order_continue
 * @property string|null $order_comment
 * @property int $user_id
 * @property string|null $d_update
 * @property string|null $food
 * @property string|null $record_io
 * @property array|null $labs
 * @property array|null $xrays
 * @property array|null $specialist_progress_note
 */
class DoctorOrders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $subjective;
    public $objective;
    public $assessment;
    public $plan;

    public static function tableName()
    {
        return 'doctor_orders';
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'labs',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'labs',
                ],
                'value' => function ($event) {
                    return $this->labs ? implode(',', $this->labs) : $this->labs;
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'xrays',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'xrays',
                ],
                'value' => function ($event) {
                    return $this->xrays ? implode(',', $this->xrays) : $this->xrays;
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'specialist_progress_note',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'specialist_progress_note',
                ],
                'value' => function ($event) {
                    
                    $this->subjective ? $s = implode(',',$this->subjective): $s = '';
                    $this->objective ? $o = implode(',',$this->objective): $o='';
                    $this->assessment ? $a = implode(',',$this->assessment):$a = '';
                    $this->plan ? $p = implode(',',$this->plan):$p='';
                    $this->specialist_progress_note = 'S:'.$s.PHP_EOL.'O:'.$o.PHP_EOL.'A:'.$a.PHP_EOL.'P:'.$p.PHP_EOL;
                    return $this->specialist_progress_note;
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'order_datetime', 'user_id'], 'required'],
            [['an', 'user_id','receive_id','pharmacist_id','approve_order_id'], 'integer'],
            [['order_datetime', 'labs', 'xrays', 'd_update', 'specialist_progress_note','image','subjective','objective','assessment','plan'], 'safe'],
            [['progress_note', 'order_oneday', 'order_continue', 'order_comment', 'extra_food'], 'string'],
            [['ward'], 'string', 'max' => 2],
            [['bed_no', 'order_status'], 'string', 'max' => 10],
            [['food'], 'string', 'max' => 30],
            [['record_io', 'record_vitalsign'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'an' => 'An',
            'ward' => 'Ward',
            'bed_no' => 'Bed No.',
            'order_datetime' => 'Order Datetime',
            'progress_note' => 'Progress Note',
            'order_oneday' => 'Order Oneday',
            'order_continue' => 'Order Continue',
            'order_comment' => 'หมายเหตุ',
            'user_id' => 'User ID',
            'd_update' => 'D Update',
            'food' => 'อาหาร',
            'record_io' => 'I/O',
            'record_vitalsign' => 'V/S',
            'order_status' => 'สถานะ',
            'chest_xray' => 'Chest X-Ray',
            'cbc' => 'CBC',
            'elyte' => 'Electrolyte',
            'creatinine' => 'Creatinine',
            'bun' => 'BUN',
            'hct' => 'HCT',
            'dtx' => 'DTX',
            'extra_food' => 'อื่นๆ',
            'specialist_progress_note' => 'Progress Note สาขาเฉพาะ'
        ];
    }

    public static function itemsAlias($key)
    {
        $items = [
            'food' => [
                'Regular Diet' => 'Regular Diet',
                'Soft Diet' => 'Soft Diet',
                'Liquid Diet' => 'Liquid Diet',
                'Tube Feed' => 'Tube Feed',
            ],
            'labs' => ArrayHelper::map(LStandingOrder::find()->where(['is_active' => '1','user_id'=>Yii::$app->user->identity->id])->all(),'standing_order_id','standing_order_name'),
            'xrays' => [],            
            'standing' => ArrayHelper::map(LStandingOrder::find()->where(['is_active' => '1','user_id'=>Yii::$app->user->identity->id])->all(),'standing_order_id','standing_order_name'),
            'med' => ArrayHelper::map(LProgressNote::find()->where(['specialist_group_code' => 'med'])->all(),'progress_value','progress_value'),
            'surgery' => ArrayHelper::map(LProgressNote::find()->where(['specialist_group_code' => 'surgery'])->all(),'progress_value','progress_value'),
            'eye' => ArrayHelper::map(LProgressNote::find()->where(['specialist_group_code' => 'eye'])->all(),'progress_value','progress_value'),
            'ortho' => ArrayHelper::map(LProgressNote::find()->where(['specialist_group_code' => 'ortho'])->all(),'progress_value','progress_value'),

        ];
        return ArrayHelper::getValue($items, $key, []);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemFood()
    {
        return self::itemsAlias('food');
    }

    public function getItemLabs()
    {
        return self::itemsAlias('labs');
    }

    public function getItemXrays()
    {
        return self::itemsAlias('xrays');
    }

    public function getItemMed()
    {
        return self::itemsAlias('med');
    }

    public function getItemSurgery()
    {
        return self::itemsAlias('surgery');
    }

    public function getItemEye()
    {
        return self::itemsAlias('eye');
    }

    public function getItemOrtho()
    {
        return self::itemsAlias('ortho');
    }

    public function getFoodName()
    {
        return ArrayHelper::getValue($this->getItemFood(), $this->food);
    }

    public function getLabsName()
    {
        $lab = $this->getItemLabs();
        $labSelected = explode(',', $this->labs);
        $labSelectedName = [];
        foreach ($lab as $key => $labName) {
            foreach ($labSelected as $labKey) {
                if ($key === $labKey) {
                    $labSelectedName[] = $labName;
                }
            }
        }
        return implode('<br>', $labSelectedName);
    }

    public function xraysToArray()
    {
      return $this->xrays = explode(',', $this->xrays);
    }

    public function getXraysName()
    {
        $xray = $this->getItemXrays();
        $xraySelected = explode(',', $this->xrays);
        $xraySelectedName = [];
        foreach ($xray as $key => $xrayName) {
            foreach ($xraySelected as $xrayKey) {
                if ($key === $xrayKey) {
                    $xraySelectedName[] = $xrayName;
                }
            }
        }
        return implode('<br>', $xraySelectedName);
    }

    public function labsToArray()
    {
      return $this->labs = explode(',', $this->labs);
    }

    public function getProgName()
    {
        $surgery = $this->getItemSurgery();
        $ortho = $this->getItemortho();
        $eye = $this->getItemEye();
        $med = $this->getItemmed();

        $progSelected = explode(',', $this->specialist_progress_note);
        $progSelectedName = [];
        foreach ($med as $key => $progName) {
            foreach ($progSelected as $progKey) {
                if ($key === $progKey) {
                    $progSelectedName[] = $progName;
                }
            }
        }
        foreach ($surgery as $key => $progName) {
            foreach ($progSelected as $progKey) {
                if ($key === $progKey) {
                    $progSelectedName[] = $progName;
                }
            }
        }
        foreach ($ortho as $key => $progName) {
            foreach ($progSelected as $progKey) {
                if ($key === $progKey) {
                    $progSelectedName[] = $progName;
                }
            }
        }
        foreach ($eye as $key => $progName) {
            foreach ($progSelected as $progKey) {
                if ($key === $progKey) {
                    $progSelectedName[] = $progName;
                }
            }
        }

        return implode('<br>', $progSelectedName);
    }

    public function progressToArray()
    {
      return $this->specialist_progress_note = explode(',', $this->specialist_progress_note);
    }

    public function getIdpm()
    {
        return $this->hasOne(Idpm::className(), ['idpm' => 'ward']);
    }

    public function getWardName()
    {
        return $this->idpm->nameidpm;
    }

    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }

    public function getReceive()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'receive_id']);
    }

    public function getPharmacist()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'pharmacist_id']);
    }

    public function getConfirm()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'approve_order_id']);
    }

    public function getContinueOrders()
    {
        return DoctorOrders::find()->where(['an' => $this->an])->all();
    }

    public function getStatusName()
    {
        $status = 'ยังไม่ได้รับ';
        if ($this->order_status == 'Incomplete') {
            $status = 'ได้รับไม่ครบ';
        }
        if ($this->order_status == 'Pending') {
            $status = 'ยังได้รับ';
        }
        if ($this->order_status == 'Complete') {
            $status = 'ได้รับครบ';
        }
        return $status;
    }
}
