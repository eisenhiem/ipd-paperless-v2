<?php

namespace app\controllers;

use app\models\LProgressNote;
use app\models\LProgressNoteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProgressNoteController implements the CRUD actions for LProgressNote model.
 */
class ProgressNoteController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all LProgressNote models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new LProgressNoteSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LProgressNote model.
     * @param int $progress_id Progress ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($progress_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($progress_id),
        ]);
    }

    /**
     * Creates a new LProgressNote model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new LProgressNote();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['index']);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LProgressNote model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $progress_id Progress ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($progress_id)
    {
        $model = $this->findModel($progress_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LProgressNote model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $progress_id Progress ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($progress_id)
    {
        $this->findModel($progress_id)->delete();

        return $this->redirect(['index']);
    }

    public function actionChange($progress_id)
    {
        $model = $this->findModel($progress_id);
        if($model->is_active == '1'){
            $model->is_active ='0';
        } else {
            $model->is_active ='1';            
        }
        $model->save(false);

        return $this->redirect(['index']);    
    }

    /**
     * Finds the LProgressNote model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $progress_id Progress ID
     * @return LProgressNote the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($progress_id)
    {
        if (($model = LProgressNote::findOne(['progress_id' => $progress_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
