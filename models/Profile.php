<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "profile".
 *
 * @property int $user_id
 * @property int|null $department_id
 * @property int|null $sub_department_id
 * @property string|null $fullname
 * @property string|null $cid
 * @property string|null $position
 * @property string|null $position_level
 * @property string|null $license_no
 * @property string|null $specialist
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'department_id', 'sub_department_id'], 'integer'],
            [['fullname'], 'string', 'max' => 255],
            [['cid'], 'string', 'max' => 13],
            [['position'], 'string', 'max' => 100],
            [['position_level'], 'string', 'max' => 30],
            [['license_no','specialist'], 'string', 'max' => 10],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'department_id' => 'กลุ่มงาน',
            'sub_department_id' => 'หน่วยงาน',
            'fullname' => 'ชื่อ-สกุล',
            'cid' => 'เลขบัตรประชาชน',
            'position' => 'ตำแหน่ง',
            'position_level' => 'ระดับ',
            'license_no' => 'เลขที่อนุมัติ',
            'specialist' => 'เฉพาะทาง',
        ];
    }

    public static function itemsAlias($key)
    {
        $items = [
            'spclty' => [
                'medicine' => 'อายุรกรรม',
                'surgery' => 'ศัลยกรรม', 
                'obstetrics' => 'สูติกรรม',
                'gynecology' => 'นรีเวชกรรม',
                'ent' => 'โสต ศอ นาสิก',
                'eye' => 'จักษุ', 
                'ortho' => 'ศัลยกรรมกระดูก',
                'psyche' => 'จิตเวช',
                'fammed' => 'เวชศาสตร์ครอบครัว',
                'radio' => 'รัวสีวิทยา',
                'dent' => 'ทันตกรรม',
                'rehab' => 'เวชศาสตร์ฟื้นฟู',
                'child' => 'กุมารเวชศาสตร์'
            ],
        ];
        return ArrayHelper::getValue($items, $key, []);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemSpclty()
    {
        return self::itemsAlias('spclty');
    }

    public function getSpcName()
    {
        return ArrayHelper::getValue($this->getItemSpclty(), $this->specialist);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
