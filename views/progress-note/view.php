<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LProgressNote */

$this->title = $model->progress_id;
$this->params['breadcrumbs'][] = ['label' => 'L Progress Notes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="lprogress-note-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'progress_id' => $model->progress_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'progress_id' => $model->progress_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'progress_id',
            'specialist_group_code',
            'progress_type',
            'progress_value',
            'd_update',
        ],
    ]) ?>

</div>
