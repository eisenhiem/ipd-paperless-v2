<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DoctorOrders;

/**
 * DoctorOrdersSearch represents the model behind the search form of `app\models\DoctorOrders`.
 */
class DoctorOrdersSearch extends DoctorOrders
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'an', 'user_id','receive_id','pharmacist_id','approve_order_id'], 'integer'],
            [['order_datetime', 'progress_note', 'order_oneday', 'order_continue', 'order_comment', 'order_status','ward', 'bed_no', 'd_update', 'food', 'record_io', 'record_vitalsign'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DoctorOrders::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'receive_id' => $this->receive_id,
            'pharmacist_id' => $this->pharmacist_id,
            'approve_order_id' => $this->approve_order_id,
            'an' => $this->an,
            'order_datetime' => $this->order_datetime,
            'user_id' => $this->user_id,
            'd_update' => $this->d_update,
        ]);

        $query->andFilterWhere(['like', 'progress_note', $this->progress_note])
            ->andFilterWhere(['like', 'order_oneday', $this->order_oneday])
            ->andFilterWhere(['like', 'order_continue', $this->order_continue])
            ->andFilterWhere(['like', 'order_comment', $this->order_comment])
            ->andFilterWhere(['like', 'order_status', $this->order_status])
            ->andFilterWhere(['like', 'ward', $this->ward])
            ->andFilterWhere(['like', 'food', $this->food])
            ->andFilterWhere(['like', 'record_io', $this->record_io])
            ->andFilterWhere(['like', 'record_vitalsign', $this->record_vitalsign]);

        return $dataProvider;
    }
}
