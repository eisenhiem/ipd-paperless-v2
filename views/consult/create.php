<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IptConsult */

$this->title = 'Consult';
?>
<div class="ipt-consult-create">

    <?= $this->render('_form', [
        'model' => $model,
        'an' => $an,
    ]) ?>

</div>
