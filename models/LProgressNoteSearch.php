<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LProgressNote;

/**
 * LProgressNoteSearch represents the model behind the search form of `app\models\LProgressNote`.
 */
class LProgressNoteSearch extends LProgressNote
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['progress_id'], 'integer'],
            [['specialist_group_code', 'progress_type', 'progress_value', 'd_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LProgressNote::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'progress_id' => $this->progress_id,
            'd_update' => $this->d_update,
        ]);

        $query->andFilterWhere(['like', 'specialist_group_code', $this->specialist_group_code])
            ->andFilterWhere(['like', 'progress_type', $this->progress_type])
            ->andFilterWhere(['like', 'progress_value', $this->progress_value]);

        return $dataProvider;
    }
}
