<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LStandingOrder */

$this->title = 'แก้ไข Standing Order: ' . $model->standing_order_name;

?>
<div class="lstanding-order-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
