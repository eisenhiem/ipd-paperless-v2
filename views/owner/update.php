<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IptOwner */

$this->title = 'แก้ไข: ' . $model->owner_id;

?>
<div class="ipt-owner-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
