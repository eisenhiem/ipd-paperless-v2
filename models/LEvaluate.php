<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "l_evaluate".
 *
 * @property string $evaluate_id
 * @property string|null $evaluate_name
 * @property string|null $is_active
 */
class LEvaluate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'l_evaluate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['evaluate_id'], 'required'],
            [['is_active'], 'string'],
            [['evaluate_id'], 'string', 'max' => 2],
            [['evaluate_name'], 'string', 'max' => 255],
            [['evaluate_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'evaluate_id' => 'Evaluate ID',
            'evaluate_name' => 'Evaluate Name',
            'is_active' => 'Is Active',
        ];
    }
}
