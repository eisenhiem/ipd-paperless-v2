<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "iptdr".
 *
 * @property int $id
 * @property int $an
 * @property string $dct
 * @property string $srvdttm
 */
class Iptdr extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'iptdr';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'dct', 'srvdttm'], 'required'],
            [['an'], 'integer'],
            [['srvdttm'], 'safe'],
            [['dct'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'an' => 'An',
            'dct' => 'Dct',
            'srvdttm' => 'Srvdttm',
        ];
    }
}
