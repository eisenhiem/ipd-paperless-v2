<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dct".
 *
 * @property string $dct
 * @property string $pname
 * @property string $fname
 * @property string $lname
 * @property string $dspname
 * @property string $lcno
 * @property string $psswrd
 * @property string $specialty
 * @property string $cid
 * @property string $registerno
 * @property string $council
 * @property string $sex
 * @property string|null $birth
 * @property string|null $providertype
 * @property string|null $startdate
 * @property string|null $outdate
 * @property string $movefrom
 * @property string $moveto
 * @property string $ldate
 * @property string $statusdct
 * @property string $roomor
 * @property string $admtype
 * @property string $print_flg
 * @property string $line_id
 * @property string $tel
 * @property int|null $id
 * @property string|null $pssadm
 * @property string|null $chkptlog
 * @property string|null $userxry
 * @property string|null $passxry
 * @property string|null $xread
 */
class Dct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dct';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dct', 'pname', 'fname', 'lname', 'dspname', 'lcno', 'psswrd', 'specialty', 'cid', 'registerno', 'council', 'sex', 'movefrom', 'moveto', 'ldate', 'roomor', 'admtype', 'line_id', 'tel'], 'required'],
            [['birth', 'startdate', 'outdate', 'ldate'], 'safe'],
            [['print_flg'], 'string'],
            [['id'], 'integer'],
            [['dct', 'lcno', 'providertype', 'movefrom', 'moveto'], 'string', 'max' => 5],
            [['pname', 'registerno'], 'string', 'max' => 15],
            [['fname', 'lname'], 'string', 'max' => 25],
            [['dspname'], 'string', 'max' => 50],
            [['psswrd', 'line_id', 'tel', 'userxry', 'passxry'], 'string', 'max' => 20],
            [['specialty'], 'string', 'max' => 3],
            [['cid'], 'string', 'max' => 13],
            [['council'], 'string', 'max' => 2],
            [['sex', 'statusdct', 'roomor', 'admtype', 'chkptlog', 'xread'], 'string', 'max' => 1],
            [['pssadm'], 'string', 'max' => 8],
            [['dct'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dct' => 'Dct',
            'pname' => 'Pname',
            'fname' => 'Fname',
            'lname' => 'Lname',
            'dspname' => 'Dspname',
            'lcno' => 'Lcno',
            'psswrd' => 'Psswrd',
            'specialty' => 'Specialty',
            'cid' => 'Cid',
            'registerno' => 'Registerno',
            'council' => 'Council',
            'sex' => 'Sex',
            'birth' => 'Birth',
            'providertype' => 'Providertype',
            'startdate' => 'Startdate',
            'outdate' => 'Outdate',
            'movefrom' => 'Movefrom',
            'moveto' => 'Moveto',
            'ldate' => 'Ldate',
            'statusdct' => 'Statusdct',
            'roomor' => 'Roomor',
            'admtype' => 'Admtype',
            'print_flg' => 'Print Flg',
            'line_id' => 'Line ID',
            'tel' => 'Tel',
            'id' => 'ID',
            'pssadm' => 'Pssadm',
            'chkptlog' => 'Chkptlog',
            'userxry' => 'Userxry',
            'passxry' => 'Passxry',
            'xread' => 'Xread',
        ];
    }

    public function getPrename()
    {
        return $this->hasOne(LPrename::className(),['prename_code' => 'pname']);
    }

    public function getProviderName()
    {
        return $this->prename->prename.$this->fname.' '.$this->lname;
    }

}
