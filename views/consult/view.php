<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\IptConsult */

$this->title = $model->consult_id;
$this->params['breadcrumbs'][] = ['label' => 'Ipt Consults', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ipt-consult-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'consult_id' => $model->consult_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'consult_id' => $model->consult_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'consult_id',
            'consult_type',
            'order_id',
            'consult_description:ntext',
            'user_id',
            'consult_datetime',
            'u_update',
        ],
    ]) ?>

</div>
