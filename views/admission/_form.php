<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ipt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ipt-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vn')->textInput() ?>

    <?= $form->field($model, 'an')->textInput() ?>

    <?= $form->field($model, 'hn')->textInput() ?>

    <?= $form->field($model, 'ward')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rgtdate')->textInput() ?>

    <?= $form->field($model, 'rgttime')->textInput() ?>

    <?= $form->field($model, 'pttype')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prediag')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dchdate')->textInput() ?>

    <?= $form->field($model, 'daycnt')->textInput() ?>

    <?= $form->field($model, 'dchtime')->textInput() ?>

    <?= $form->field($model, 'dchstts')->textInput() ?>

    <?= $form->field($model, 'dchtype')->textInput() ?>

    <?= $form->field($model, 'dthdiagdct')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ipdlct')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bmi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bw')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'height')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rr')->textInput() ?>

    <?= $form->field($model, 'pr')->textInput() ?>

    <?= $form->field($model, 'sbp')->textInput() ?>

    <?= $form->field($model, 'dbp')->textInput() ?>

    <?= $form->field($model, 'ipttype')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'painscore')->textInput() ?>

    <?= $form->field($model, 'symptmi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'drg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rw')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'adjrw')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'error')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'warning')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'actlos')->textInput() ?>

    <?= $form->field($model, 'grouper_version')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'wtlos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ot')->textInput() ?>

    <?= $form->field($model, 'dept')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
