<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

if(Yii::$app->user->identity->role == 2) {
    $consult = 'เวชปฏิบัติฯ';
}
if(Yii::$app->user->identity->role == 4) {
    $consult = 'เภสัช';
}
if(Yii::$app->user->identity->role == 6) {
    $consult = 'กายภาพ';
}
if(Yii::$app->user->identity->role == 7) {
    $consult = 'ทันตกรรม';
}
if(Yii::$app->user->identity->role == 8) {
    $consult = 'แพทย์เฉพาะทาง';
}

/* @var $this yii\web\View */
/* @var $model app\models\ConsultRep */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="consult-rep-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card bg-primary">
  <div class="card-header">
  </div>
  <div class="card-body bg-light">
    <?= $form->field($model, 'rep_detail')->textarea(['rows' => 6]) ?>

    <div class="card-footer bg-light">
  <div class="form-group" style="text-align: center;">
        <?= Html::submitButton(Icon::show('save').' บันทึก', ['class' => 'btn btn-success','style'=>'width:200px']) ?>
        &emsp; 
        <?= Html::a(Icon::show('chevron-left').' กลับไปหน้าหลัก', ['consult/index','type'=>$consult], ['class' => 'btn btn-danger','style'=>'width:200px']) ?>
    </div>
  </div>

    <?php ActiveForm::end(); ?>

</div>
