<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pi".
 *
 * @property int $vn
 * @property string $pi
 */
class Pi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn'], 'integer'],
            [['pi'], 'string'],
        ];
    }

    public static function primaryKey(){
        return ['vn'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'pi' => 'Pi',
        ];
    }
}
