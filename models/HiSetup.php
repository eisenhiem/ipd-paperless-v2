<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hi_setup".
 *
 * @property int $id
 * @property string|null $common_code
 * @property string|null $value1
 * @property string|null $value2
 * @property string|null $value3
 * @property string|null $values4
 * @property string|null $remark
 * @property string|null $enable_flg
 */
class HiSetup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hi_setup';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['enable_flg'], 'string'],
            [['common_code', 'value1', 'value2', 'values4'], 'string', 'max' => 100],
            [['value3', 'remark'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'common_code' => 'Common Code',
            'value1' => 'Value 1',
            'value2' => 'Value 2',
            'value3' => 'Value 3',
            'values4' => 'Values 4',
            'remark' => 'Remark',
            'enable_flg' => 'Enable Flg',
        ];
    }
}
