<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ConsultRep */

$this->title = 'Update Consult Rep: ' . $model->rep_id;
$this->params['breadcrumbs'][] = ['label' => 'Consult Reps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->rep_id, 'url' => ['view', 'rep_id' => $model->rep_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="consult-rep-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
