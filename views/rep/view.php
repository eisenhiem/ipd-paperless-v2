<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ConsultRep */

$this->title = $model->rep_id;
$this->params['breadcrumbs'][] = ['label' => 'Consult Reps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="consult-rep-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'rep_id' => $model->rep_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'rep_id' => $model->rep_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'rep_id',
            'consult_id',
            'order_id',
            'user_id',
            'rep_detail:ntext',
            'rep_datetime',
            'd_update',
        ],
    ]) ?>

</div>
