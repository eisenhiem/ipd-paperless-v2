<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LProgressNoteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lprogress-note-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'progress_id') ?>

    <?= $form->field($model, 'specialist_group_code') ?>

    <?= $form->field($model, 'progress_type') ?>

    <?= $form->field($model, 'progress_value') ?>

    <?= $form->field($model, 'd_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
