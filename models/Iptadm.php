<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "iptadm".
 *
 * @property int $an
 * @property string $indate
 * @property int $intime
 * @property string $roomno
 * @property string $bedno
 * @property string $bedtype
 * @property float $rate
 * @property int $daycnt
 * @property string $outdate
 * @property int $outtime
 * @property int $id
 */
class Iptadm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'iptadm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'intime', 'roomno', 'bedno', 'bedtype', 'rate', 'daycnt', 'outtime'], 'required'],
            [['an', 'intime', 'daycnt', 'outtime'], 'integer'],
            [['indate', 'outdate'], 'safe'],
            [['rate'], 'number'],
            [['roomno', 'bedno'], 'string', 'max' => 5],
            [['bedtype'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'An',
            'indate' => 'Indate',
            'intime' => 'Intime',
            'roomno' => 'Roomno',
            'bedno' => 'Bedno',
            'bedtype' => 'Bedtype',
            'rate' => 'Rate',
            'daycnt' => 'Daycnt',
            'outdate' => 'Outdate',
            'outtime' => 'Outtime',
            'id' => 'ID',
        ];
    }

    public function getBedName()
    {
        $bed = substr($this->bedno,2,5);
        return $bed;
    } 
}
