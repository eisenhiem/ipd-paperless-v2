<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "foodmain".
 *
 * @property string $codefood
 * @property string $namefood
 * @property string $name_s
 */
class Foodmain extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'foodmain';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codefood', 'namefood', 'name_s'], 'required'],
            [['codefood'], 'string', 'max' => 2],
            [['namefood'], 'string', 'max' => 30],
            [['name_s'], 'string', 'max' => 15],
            [['codefood'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codefood' => 'Codefood',
            'namefood' => 'Namefood',
            'name_s' => 'Name S',
        ];
    }
}
