<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IptConsult;

/**
 * IptConsultSearch represents the model behind the search form of `app\models\IptConsult`.
 */
class IptConsultSearch extends IptConsult
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['consult_id', 'order_id', 'user_id'], 'integer'],
            [['consult_type', 'consult_description', 'consult_datetime', 'u_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IptConsult::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'consult_id' => $this->consult_id,
            'order_id' => $this->order_id,
            'user_id' => $this->user_id,
            'consult_datetime' => $this->consult_datetime,
            'u_update' => $this->u_update,
        ]);

        $query->andFilterWhere(['like', 'consult_type', $this->consult_type])
            ->andFilterWhere(['like', 'consult_description', $this->consult_description]);

        return $dataProvider;
    }
}
