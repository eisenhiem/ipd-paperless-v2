<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ConsultRep;

/**
 * ConsultRepSearch represents the model behind the search form of `app\models\ConsultRep`.
 */
class ConsultRepSearch extends ConsultRep
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rep_id', 'consult_id', 'order_id', 'user_id'], 'integer'],
            [['rep_detail', 'rep_datetime', 'd_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ConsultRep::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'rep_id' => $this->rep_id,
            'consult_id' => $this->consult_id,
            'order_id' => $this->order_id,
            'user_id' => $this->user_id,
            'rep_datetime' => $this->rep_datetime,
            'd_update' => $this->d_update,
        ]);

        $query->andFilterWhere(['like', 'rep_detail', $this->rep_detail]);

        return $dataProvider;
    }
}
