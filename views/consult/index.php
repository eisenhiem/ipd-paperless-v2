<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IptConsultSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ipt Consults';
?>
<div class="ipt-consult-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'consult_id',
            //'consult_type',
            //'order_id',
            [
                'header' => 'AN',
                'value' => function($model){
                    return $model->order->an;
                }
            ],
            'consult_description:ntext',
            [
                'attribute' => 'user_id',
                'value' => function($model){
                    return $model->profile->fullname;
                }
            ],
            //'user_id',
            //'consult_datetime',
            //'u_update',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'ตอบกลับ',
                'options'=>['style'=>'width:100px;'],
                'template'=>'{reply} {order}',
                'buttons'=>[
                  'reply' => function($url,$model,$key){
                    return Html::a(Icon::show('reply'), ['rep/create','id'=>$model->consult_id], ['class' => 'btn btn-success','style' =>['width'=>'60px']]) ;
                  },
                  'order' => function($url,$model,$key){
                    return Html::a(Icon::show('notes-medical'), ['order','id'=>$model->consult_id], ['class' => 'btn btn-info','style' =>['width'=>'60px']]) ;
                  }

                ]
              ],
        ],
    ]); ?>


</div>
