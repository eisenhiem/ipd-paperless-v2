<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "incoth".
 *
 * @property int $vn
 * @property int $an
 * @property string $date
 * @property int $time
 * @property string $income
 * @property string $pttype
 * @property string $paidst
 * @property int $rcptno
 * @property float $rcptamt
 * @property int $recno
 * @property string $cgd
 * @property string $codecheck
 * @property int $ln
 * @property int $id
 */
class Incoth extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'incoth';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'an', 'time', 'income', 'pttype', 'paidst', 'rcptno', 'rcptamt', 'recno', 'cgd', 'codecheck', 'ln'], 'required'],
            [['vn', 'an', 'time', 'rcptno', 'recno', 'ln'], 'integer'],
            [['date'], 'safe'],
            [['rcptamt'], 'number'],
            [['income', 'pttype'], 'string', 'max' => 2],
            [['paidst'], 'string', 'max' => 1],
            [['cgd'], 'string', 'max' => 5],
            [['codecheck'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'an' => 'An',
            'date' => 'Date',
            'time' => 'Time',
            'income' => 'Income',
            'pttype' => 'Pttype',
            'paidst' => 'Paidst',
            'rcptno' => 'Rcptno',
            'rcptamt' => 'Rcptamt',
            'recno' => 'Recno',
            'cgd' => 'Cgd',
            'codecheck' => 'Codecheck',
            'ln' => 'Ln',
            'id' => 'ID',
        ];
    }
}
