<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LEvaluate */

$this->title = 'Update L Evaluate: ' . $model->evaluate_id;
?>
<div class="levaluate-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
