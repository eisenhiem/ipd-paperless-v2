<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ipt_owner".
 *
 * @property int $owner_id
 * @property int $an
 * @property string $owner_type
 * @property int $owner_user_id
 * @property string|null $owner_status
 * @property string|null $d_update
 */
class IptOwner extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ipt_owner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'owner_type', 'owner_user_id'], 'required'],
            [['an', 'owner_user_id'], 'integer'],
            [['owner_status'], 'string'],
            [['d_update'], 'safe'],
            [['owner_type'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'owner_id' => 'Owner ID',
            'an' => 'AN',
            'owner_type' => 'ประเภท',
            'owner_user_id' => 'แพทย์',
            'owner_status' => 'สถานะ',
            'd_update' => 'D Update',
        ];
    }

    
    public static function itemsAlias($key){
        $items = [
          'type'=> [
            'owner' => ' แพทย์เจ้าของไข้ ',
            'consult' => ' แพทย์ Consult',
          ],

        ];
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemType()
    {
      return self::itemsAlias('type');
    }

    public function getTypeName(){
        return ArrayHelper::getValue($this->getItemType(),$this->owner_type);
    }

    public function getProfile()
    {
        return $this->hasOne(Profile::className(),['user_id' => 'owner_user_id']);
    }
}
