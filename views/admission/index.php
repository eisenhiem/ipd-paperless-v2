<?php

use app\models\IptOwner;
use kartik\icons\Icon;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AdmissionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูลผู้ป่วย ' . $ward->nameidpm;
?>
<div class="admission-index">

    <div style="text-align:right">
        <?= Html::a(Icon::show('table') . 'ตาราง', ['switchlayout', 'id' => $ward->idpm], ['class' => 'btn btn-primary']) ?>
        <?php echo $this->render('_search', ['model' => $searchModel, 'id' => $ward->idpm]); ?>
    </div>
    <div class="row">
        <?php
        foreach ($model as $patient) {
            $owner = IptOwner::find()->where(['an' => $patient->an, 'owner_status' => '1'])->all();
            $ownername = '';
            if ($owner) {
                foreach ($owner as $o) {
                    $ownername = $ownername . $o->getTypeName() . ':' . $o->profile->fullname . '<br>';
                }
            } else {
                $ownername = 'ยังไม่ระบุแพทย์';
            }
        ?>
            <div class="col-md-6 col-lg-4">
                <div <?= $patient->pt->getGender() == 'ชาย' ? 'class="card text-black" style="background:#C4DDFF"' : 'class="card text-black " style="background:#F9CEEE"' ?>>
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <b>AN <?= $patient->an ?>
                            </div>
                            <div class="col-6">
                                <div style="text-align:right">เตียง <?= $patient->iptadm->getBedName() ? $patient->iptadm->getBedName() : 'ยังไม่ระบุเตียง' ?></div></b>
                            </div>
                            <div class="col-10">
                                <?= $ownername ?>
                            </div>
                            <div class="col-2">
                                <?= Html::a(Icon::show('plus'), ['owner/index', 'an' => $patient->an, 'ward' => $ward->idpm], ['class' => 'btn btn-success']) ?>
                            </div>
                        </div>

                    </div>
                    <div class="card-body">
                        <b>HN : <?= $patient->hn ?> </b> <br>
                        <b>ชื่อ-สกุล :</b> <?= $patient->pt->getFullName() ?>
                        <br>
                        <b>เลขบัตรประชาชน :</b> <?= $patient->pt->pop_id ?>
                        <br>
                        <b>เพศ : </b><?= $patient->pt->getGender() ?> &emsp; <b>อายุ : </b><?= $patient->pt->getAge() ?>
                        <br>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-4">
                                <?= Html::a('ดูข้อมูล', ['view', 'an' => $patient->an], ['class' => 'btn btn-info btn-block']) ?>
                            </div>
                            <div class="col-4">
                                <?= Html::a('Orders', ['order/list', 'id' => $patient->an], ['class' => $patient->getReceive()]) ?>
                            </div>
                            <div class="col-4">
                                <?= Html::a('Notes', ['note/list', 'id' => $patient->an], ['class' => 'btn btn-warning btn-block']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
</div>