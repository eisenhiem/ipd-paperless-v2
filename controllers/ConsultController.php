<?php

namespace app\controllers;

use app\models\DoctorOrders;
use app\models\IptConsult;
use app\models\IptConsultSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

date_default_timezone_set('Asia/Bangkok');
/**
 * ConsultController implements the CRUD actions for IptConsult model.
 */
class ConsultController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all IptConsult models.
     *
     * @return string
     */
    public function actionIndex($type)
    {
        $searchModel = new IptConsultSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        $dataProvider->query->andWhere(['consult_type' => $type]);
        $dataProvider->query->andWhere(['consult_status' => '0']);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single IptConsult model.
     * @param int $consult_id Consult ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($consult_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($consult_id),
        ]);
    }

    /**
     * Creates a new IptConsult model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($id)
    {
        $model = new IptConsult();
        $order = DoctorOrders::find()->where(['order_id' => $id])->one(); 
        $model->order_id = $id;
        $model->user_id = Yii::$app->user->identity->id;
        $model->consult_datetime = date('Y-m-d H:i:s');
        $model->consult_type = '1';

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                $order->order_comment = $order->order_comment . PHP_EOL.'Consult:' .$model->consult_type .PHP_EOL.$model->consult_description;
                $order->labsToArray();
                $order->xraysToArray();
                $order->save();
                return $this->redirect(['order/list','id'=>$order->an]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'an' => $order->an,
        ]);
    }

    public function actionOrder($id)
    {
        $model = $this->findModel($id);
        $model->consult_type = '1';
        $model->save();                
        return $this->redirect(['order/list','id'=>$model->order->an]);
    }


    /**
     * Updates an existing IptConsult model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $consult_id Consult ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($consult_id)
    {
        $model = $this->findModel($consult_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'consult_id' => $model->consult_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing IptConsult model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $consult_id Consult ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($consult_id)
    {
        $this->findModel($consult_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IptConsult model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $consult_id Consult ID
     * @return IptConsult the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($consult_id)
    {
        if (($model = IptConsult::findOne(['consult_id' => $consult_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
