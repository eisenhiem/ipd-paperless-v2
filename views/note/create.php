<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NurseNotes */

$this->title = 'บันทึก Nurse Notes';
?>
<div class="nurse-notes-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
