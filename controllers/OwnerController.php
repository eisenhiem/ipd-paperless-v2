<?php

namespace app\controllers;

use app\models\IptOwner;
use app\models\IptOwnerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OwnerController implements the CRUD actions for IptOwner model.
 */
class OwnerController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all IptOwner models.
     *
     * @return string
     */
    public function actionIndex($an,$ward)
    {
        $searchModel = new IptOwnerSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        $dataProvider->query->andWhere(['an'=>$an]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'an' => $an,
            'ward' => $ward,
        ]);
    }

    /**
     * Displays a single IptOwner model.
     * @param int $owner_id Owner ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($owner_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($owner_id),
        ]);
    }

    /**
     * Creates a new IptOwner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($an,$ward)
    {
        $model = new IptOwner();
        $model->an = $an;

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['admission/index', 'ward_id' => $ward]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing IptOwner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $owner_id Owner ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($owner_id)
    {
        $model = $this->findModel($owner_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'owner_id' => $model->owner_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing IptOwner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $owner_id Owner ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($owner_id)
    {
        $this->findModel($owner_id)->delete();

        return $this->redirect(['index']);
    }

    public function actionChange($id)
    {
        $model = $this->findModel($id);
        if($model->owner_status == '1'){
            $model->owner_status ='0';
        } else {
            $model->owner_status ='1';            
        }
        $model->save(false);

        return $this->redirect(['index']);    
    }

    /**
     * Finds the IptOwner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $owner_id Owner ID
     * @return IptOwner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($owner_id)
    {
        if (($model = IptOwner::findOne(['owner_id' => $owner_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
