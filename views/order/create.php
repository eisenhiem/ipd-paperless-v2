<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DoctorOrders */

$this->title = 'Orders:'.$admit->pt->getFullName();
?>
<div class="doctor-orders-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
