<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NurseNotesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nurse Notes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nurse-notes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Nurse Notes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'note_id',
            'note_date',
            'note_time',
            'activity:ntext',
            'evaluate:ntext',
            //'user_id',
            //'d_update',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'note_id' => $model->note_id]);
                 }
            ],
        ],
    ]); ?>


</div>
