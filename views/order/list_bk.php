<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Admission;
use app\models\Ipt;
use app\models\Labresult;
use app\models\Lbbk;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AdmissionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Order ชื่อ-สกุล:' . $ward->pt->getFullName();
?>
<div class="admission-index">

    <p>
        <?= Html::a(Icon::show('chevron-left') . 'กลับ', ['admission/index', 'ward_id' => $ward->ward], ['class' => 'btn btn-danger']) ?>
        <?= $ward->dchdate == '0000-00-00' && (Yii::$app->user->identity->role == 5 || Yii::$app->user->identity->role == 9) ? Html::a(Icon::show('plus') . 'เพิ่ม Order', ['create', 'an' => $an], ['class' => 'btn btn-success']) : '' ?>
        <?php //echo  $order->value1 == 'asc' ? Html::a(Icon::show('sort-amount-down').'เรียง Z > A', ['switchsort','id' => $an], ['class' => 'btn btn-primary']):Html::a(Icon::show('sort-amount-down-alt').'เรียง A > Z', ['switchsort','id' => $an], ['class' => 'btn btn-primary']); 
        ?>
        <?= Html::a(Icon::show('flask') . 'Lab', ['admission/lab', 'id' => $an], ['class' => 'btn btn-warning']) ?>
        <?= Html::a(Icon::show('thermometer-half') . 'V/S', ['note/vitalsign', 'id' => $an], ['class' => 'btn btn-info']) ?>
        <?= Html::a(Icon::show('notes-medical') . 'Nurse Note ', ['note/list', 'id' => $an], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Icon::show('print') . ' พิมพ์ Doctor Order', ['print', 'id' => $an], ['class' => 'btn btn-warning', 'target' => '_blank']) ?>
    </p>

    <table width="100%" border="1">
        <tr>
            <th width="20%" style="text-align:center">Progress Note</th>
            <th width="35%" style="text-align:center">Order One Day</th>
            <th width="35%" style="text-align:center">Order Continue</th>
            <th width="10%" style="text-align:center">หมายเหตุ</th>
        </tr>
        <?php
        $i = 1;
        if ($model) {
            foreach ($model as $order) {
        ?>
                <tr>
                    <td style="vertical-align:top;padding-left: 10px;">
                        <?= Ipt::getThaiDateTime($order->order_datetime) ?><br><br>
                        <?= $order->specialist_progress_note ? nl2br($order->specialist_progress_note) : '' ?>
                        <?= $order->progress_note ? nl2br($order->progress_note) : '' ?><br><br>
                        <?= $order->profile->fullname ?><br>
                        <?= $order->profile->position ?><br>
                        <?= $order->profile->license_no ?><br>
                    </td>
                    <td style="vertical-align:top;padding-left: 10px;padding-right: 5px;">
                        <?= $order->order_oneday ? nl2br($order->order_oneday) : '' ?>

                        <?php
                        $lab = Lbbk::find()->where(['an' => $an])->andWhere(['senddate' => substr($order->order_datetime, 0, 10)])->groupBy('labcode')->all();
                        foreach ($lab as $l) {
                        ?>
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header bg-primary text-left" id="heading<?= $l->labcode ?>">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse<?= $l->labcode ?>" aria-expanded="false" aria-controls="collapse<?= $l->labcode ?>" style="color:white;">
                                                Lab : <?= $l->lab->labname ?> <?= $l->finish == '0' ? '<span class="right badge badge-light">ยังไม่เสร็จ</span>' : '' ?>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapse<?= $l->labcode ?>" class="collapse" aria-labelledby="heading<?= $l->labcode ?>" data-parent="#accordion">
                                        <div class="card-body">
                                            <ul>
                                                <?php
                                                $labno = Lbbk::find()->where(['vn' => $l->vn, 'labcode' => $l->labcode])->orderBy(['ln' => SORT_DESC])->all();
                                                foreach ($labno as $r) {
                                                    $result = Labresult::find()->where(['ln' => $r->ln])->orderBy(['ln' => SORT_DESC])->all();
                                                    echo 'วันที่' . $r->getServdate();
                                                    echo '<div class="row">';
                                                    foreach ($result as $v) {
                                                        echo '<div class="col-12">' . $v->lab_code_local . ':' . $v->labresult . ' ' . $v->unit . '[' . $v->normal . ']' . '</div>';
                                                    }
                                                    echo '</div>';
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </td>
                    <td style="vertical-align:top;padding-left: 10px;">
                        <?= $order->food ? $order->getFoodName() . '<br>' : '' ?>
                        <?= $order->record_io ? '&emsp; [ / ] Record I/O <br>' : '' ?>
                        <?= $order->record_vitalsign ? '&emsp; [ / ] Record V/S <br>' : '' ?>
                        <?php
                        if ($continue && $i == 1) {
                            foreach ($continue as $c) {
                                echo Ipt::getThaiDateTime($c->order_datetime) . '<br>';
                                echo '&emsp;' . nl2br($c->order_continue) . '<br>';
                            }
                        } ?>
                        <?= $order->order_continue && $i != 1 ? '<br>' . nl2br($order->order_continue) : '' ?>
                    </td>
                    <td style="vertical-align:top;padding-left: 5px;;padding-right: 5px;">
                        <?= ((Yii::$app->user->identity->id == $order->user_id) && ($ward->dchdate == '0000-00-00') && $i == 1) ? Html::a(Icon::show('edit') . ' แก้ไข', ['update', 'order_id' => $order->order_id], ['class' => 'btn btn-warning btn-block']) : '' ?>
                        <?= Html::a(Icon::show('print') . ' พิมพ์', ['order', 'id' => $order->order_id], ['class' => 'btn btn-info btn-block', 'target' => '_blank']) ?><br>
                        <?= (($ward->dchdate == '0000-00-00') && $i == 1) ? Html::a(Icon::show('plus') . ' Consult', ['consult/create', 'id' => $order->order_id], ['class' => 'btn btn-success btn-block']) : '' ?><br>
                        <?= nl2br($order->order_comment) ?>
                    </td>
                </tr>
        <?php
                $i++;
            }
        }
        ?>
    </table>
    <br>
    <div style="text-align:center">
        <?= !$ward->dchdate == '0000-00-00' ? Html::a('Discharge คนไข้', ['admission/dc', 'id' => $an], ['class' => 'btn btn-danger', 'style' => 'width:200px']) : '' ?>
    </div>
</div>