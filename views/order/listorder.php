<?php

use app\models\Ipt;
use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
//use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DoctorOrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Doctor Orders';
?>
<div class="doctor-orders-index">

    <?php echo $this->render('_search', ['model' => $searchModel,'ward_id' => $ward_id]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' =>[
          'before'=>' '
        ],
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'order_id',
            'an',
            [
              'attribute' => 'order_datetime',
              'value' => function($model){
                return Ipt::getThaiDateTime($model->order_datetime);
              }
            ],
            //'order_datetime',
            //'progress_note:ntext',
            'order_oneday:ntext',
            [
              'attribute' => 'order_continue',
              'value' => function($model){
                $orders = $model->getContinueOrders();
                $continue = '';
                foreach($orders as $c){
                  $continue = $continue . Ipt::getThaiDateTime($c->order_datetime) .'<br>' . '&emsp;'.nl2br($c->order_continue).'<br>';
                }
                return $continue;
              },
              'format' =>'raw'
            ],
            'order_comment:ntext',
            [
              'attribute' => 'order_status',
              'value' => function($model){
                return $model->getStatusName();
              }
            ],
            //'order_status',
            //'user_id',
            //'d_update',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Order',
                'options'=>['style'=>'width:60px;'],
                'template'=>'{order} {complete} {some} {print}',
                'buttons'=>[
                  'order' => function($url,$model,$key){
                    return Html::a(Icon::show('notes-medical'), ['order/create','an'=>$model->an], ['class' => 'btn btn-primary','style' =>['width'=>'60px']]) ;
                  },
                  'complete' => function($url,$model,$key){
                    return $model->order_status != 'Complete' ? Html::a(Icon::show('clipboard-check'), ['complete','order_id'=>$model->order_id], ['class' => 'btn btn-success','style' =>['width'=>'60px']]):'' ;
                  },
                  'some' => function($url,$model,$key){
                    return $model->order_status != 'Complete' ? Html::a(Icon::show('exclamation'), ['incomplete','order_id'=>$model->order_id], ['class' => 'btn btn-warning','style' =>['width'=>'60px']]):'' ;
                  },
                  'print' => function($url,$model,$key){
                    return Html::a(Icon::show('print'), ['order','id'=>$model->order_id], ['class' => 'btn btn-info','style' =>['width'=>'60px'],'target' => '_blank']);
                  },
                ]
              ],
            ],
    ]); ?>


</div>
