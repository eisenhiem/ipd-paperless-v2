<?php

namespace app\controllers;

use app\models\LStandingOrder;
use app\models\LStandingOrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StandingOrderController implements the CRUD actions for LStandingOrder model.
 */
class StandingOrderController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all LStandingOrder models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new LStandingOrderSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LStandingOrder model.
     * @param int $standing_order_id Standing Order ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($standing_order_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($standing_order_id),
        ]);
    }

    /**
     * Creates a new LStandingOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new LStandingOrder();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'standing_order_id' => $model->standing_order_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LStandingOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $standing_order_id Standing Order ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($standing_order_id)
    {
        $model = $this->findModel($standing_order_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'standing_order_id' => $model->standing_order_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LStandingOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $standing_order_id Standing Order ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($standing_order_id)
    {
        $this->findModel($standing_order_id)->delete();

        return $this->redirect(['index']);
    }

    public function actionChange($standing_order_id)
    {
        $model = $this->findModel($standing_order_id);
        if($model->is_active == '1'){
            $model->is_active ='0';
        } else {
            $model->is_active ='1';            
        }
        $model->save(false);

        return $this->redirect(['index']);    
    }

    /**
     * Finds the LStandingOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $standing_order_id Standing Order ID
     * @return LStandingOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($standing_order_id)
    {
        if (($model = LStandingOrder::findOne(['standing_order_id' => $standing_order_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
