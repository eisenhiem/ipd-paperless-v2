<?php

namespace app\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "nurse_notes".
 *
 * @property int $note_id
 * @property string $note_date
 * @property string|null $note_time
 * @property string $activity
 * @property string|null $evaluate
 * @property int $user_id
 * @property string|null $d_update
 */
class NurseNotes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nurse_notes';
    }

    public $is_save;

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'activity_select',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'activity_select',
                ],
                'value' => function ($event) {
                    return $this->activity_select ? implode(',', $this->activity_select):$this->activity_select;
                },

            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'evaluate_select',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'evaluate_select',
                ],
                'value' => function ($event) {
                    return $this->evaluate_select ? implode(',', $this->evaluate_select):$this->evaluate_select;
                },

            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['note_date', 'user_id'], 'required'],
            [['note_date', 'd_update','activity_select', 'evaluate_select'], 'safe'],
            [['activity', 'evaluate', 'problem'], 'string'],
            [['user_id','an'], 'integer'],
            [['note_time'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'note_id' => 'Note ID',
            'an' => 'AN',
            'note_date' => 'Note Date',
            'note_time' => 'Note Time',
            'problem' => 'ปัญหา',
            'activity' => 'กิจกรรม',
            'evaluate' => 'การประเมิน',
            'activity_select' => 'กิจกรรมการพยาบาล',
            'evaluate_select' => 'การประเมิน',
            'user_id' => 'ผู้บันทึก',
            'd_update' => 'D Update',
        ];
    }

    public static function itemsAlias($key){
        $items = [
          'act'=> ArrayHelper::map(LActivity::find()->all(), 'activity_id', 'activity_name'),
          'act_use' => ArrayHelper::map(LActivity::find()->where(['is_active' => '1'])->all(), 'activity_id', 'activity_name'),
          'eva'=> ArrayHelper::map(LEvaluate::find()->all(), 'evaluate_id', 'evaluate_name'),
          'eva_use' => ArrayHelper::map(LEvaluate::find()->where(['is_active' => '1'])->all(), 'evaluate_id', 'evaluate_name'),
        ];
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemAct()
    {
      return self::itemsAlias('act');
    }

    public function getActUse()
    {
      return self::itemsAlias('act_use');
    }

    public function actToArray()
    {
      return $this->activity_select = explode(',', $this->activity_select);
    }

    public function evaToArray()
    {
      return $this->evaluate_select = explode(',', $this->evaluate_select);
    }

    public function getActName(){
        $acts = $this->getItemAct();
        $actSelected = explode(',', $this->activity_select);
        $actSelectedName = [];
        foreach ($acts as $key => $actName) {
          foreach ($actSelected as $actKey) {
            if($key === $actKey){
              $actSelectedName[] = $actName;
            }
          }
        }   
        return implode('<br>', $actSelectedName);
    }

    public function getItemEva()
    {
      return self::itemsAlias('eva');
    }

    public function getEvaUse()
    {
      return self::itemsAlias('eva_use');
    }

    public function getEvaName(){
        $evas = $this->getItemEva();
        $evaSelected = explode(',', $this->evaluate_select);
        $evaSelectedName = [];
        foreach ($evas as $key => $evaName) {
          foreach ($evaSelected as $evaKey) {
            if($key === $evaKey){
              $evaSelectedName[] = $evaName;
            }
          }
        }   
        return implode('<br> &emsp;', $evaSelectedName);
    }

    public function getProfile()
    {
        return $this->hasOne(Profile::className(),['user_id' => 'user_id']);
    }

}
