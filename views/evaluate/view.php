<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LEvaluate */

$this->title = $model->evaluate_id;
\yii\web\YiiAsset::register($this);
?>
<div class="levaluate-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->evaluate_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->evaluate_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'evaluate_id',
            'evaluate_name',
            'is_active',
        ],
    ]) ?>

</div>
