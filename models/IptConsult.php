<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ipt_consult".
 *
 * @property int $consult_id
 * @property string $consult_type
 * @property int $order_id
 * @property string $consult_description
 * @property int $user_id
 * @property string $consult_datetime
 * @property string|null $u_update
 */
class IptConsult extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ipt_consult';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['consult_type', 'order_id', 'consult_description', 'user_id', 'consult_datetime'], 'required'],
            [['order_id', 'user_id'], 'integer'],
            [['consult_description'], 'string'],
            [['consult_datetime', 'u_update'], 'safe'],
            [['consult_type'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'consult_id' => 'Consult ID',
            'consult_type' => 'Consult Type',
            'order_id' => 'Order ID',
            'consult_description' => 'รายละเอียด',
            'user_id' => 'ผู้บันทึก',
            'consult_datetime' => 'วันที่เวลา',
            'u_update' => 'U Update',
        ];
    }

    public function getProfile(){
        return $this->hasOne(Profile::className(),['user_id' => 'user_id']);
    }

    public function getOrder(){
        return $this->hasOne(DoctorOrders::className(),['order_id' => 'order_id']);
    }

}
