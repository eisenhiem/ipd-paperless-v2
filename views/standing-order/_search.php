<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LStandingOrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lstanding-order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'standing_order_id') ?>

    <?= $form->field($model, 'standing_order_name') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'standing_order_type') ?>

    <?= $form->field($model, 'standing_order_map_code') ?>

    <?php // echo $form->field($model, 'medusage') ?>

    <?php // echo $form->field($model, 'qty') ?>

    <?php // echo $form->field($model, 'is_active') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
