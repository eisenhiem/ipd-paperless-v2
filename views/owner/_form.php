<?php

use app\models\Profile;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$owner = ArrayHelper::map(Profile::find()->joinWith('user')->where('user.role in (5,8)')->all(), 'user_id', 'fullname');

/* @var $this yii\web\View */
/* @var $model app\models\IptOwner */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="ipt-owner-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card bg-primary col-md-6 offset-md-3 ">
        <div class="card-header">
            <?= $form->field($model, 'owner_type')->radioList($model->getItemType())->label(false) ?>
        </div>
        <div class="card-body bg-light">
            <?= $form->field($model, 'owner_user_id')->dropDownList($owner) ?>
        </div>
        <div class="card-footer bg-light">
            <div class="form-group">
                <?= Html::submitButton(Icon::show('save') . 'บันทึก', ['class' => 'btn btn-success btn-block']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
        <br>
    </div>
</div>