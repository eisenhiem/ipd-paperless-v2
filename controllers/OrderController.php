<?php

namespace app\controllers;

use app\models\DoctorOrders;
use app\models\DoctorOrdersSearch;
use app\models\HiSetup;
use app\models\Ipt;
use app\models\Setup;
use DateTime;
use kartik\mpdf\Pdf;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

date_default_timezone_set('Asia/Bangkok');

/**
 * OrderController implements the CRUD actions for DoctorOrders model.
 */
class OrderController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ]
                    ]
                ]
            ]
        );
    }

    /**
     * Lists all DoctorOrders models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new DoctorOrdersSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Orders where AN .
     * @return mixed
     */
    public function actionList($id)
    {
        $order = HiSetup::find()->where(['common_code' => 'order_sort'])->one();
        $model = DoctorOrders::find()->where(['an' => $id])->orderBy(['order_id' => SORT_DESC])->all();
        $continue = DoctorOrders::find()->where(['an' => $id])->all();
        $ward = Ipt::find()->where(['an' => $id])->one();
        $an = $id;
        return $this->render('list', [
            'model' => $model,
            'an' => $an,
            'ward' => $ward,
            'order' => $order,
            'continue' => $continue,
        ]);
    }

    public function actionListOrder($ward_id)
    {
        $params = Yii::$app->request->queryParams;
        $searchModel = new DoctorOrdersSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        if(!$params["DoctorOrdersSearch"]){
            $dataProvider->query->andWhere('order_status <> "Complete" or date(order_datetime) = date(now())')->andWhere(['ward' => $ward_id]);
            $dataProvider->query->orderBy(['order_id' => SORT_ASC]);
        } else {
            $dataProvider->query->orderBy(['order_id' => SORT_DESC]);
        }

        return $this->render('listorder', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ward_id' => $ward_id,
        ]);
    }

    public function actionComplete($order_id)
    {
        $model = $this->findModel($order_id);
        $model->labsToArray();
        $model->xraysToArray();
        $model->order_status = 'Complete';
        $model->pharmacist_id = Yii::$app->user->identity->id;
        $model->save();

        return $this->redirect(['list-order', 'ward_id' => $model->ward]);
    }

    public function actionIncomplete($order_id)
    {
        $model = $this->findModel($order_id);
        $model->labsToArray();
        $model->xraysToArray();
        $model->order_status = 'Incomplete';
        $model->pharmacist_id = Yii::$app->user->identity->id;
        $model->save();

        return $this->redirect(['list-order', 'ward_id' => $model->ward]);
    }

    public function actionReceive($order_id)
    {
        $model = $this->findModel($order_id);
        $model->labsToArray();
        $model->xraysToArray();
        $model->order_status = 'Receive';
        $model->receive_id = Yii::$app->user->identity->id;
        $model->save();

        return $this->redirect(['list-order', 'ward_id' => $model->ward]);
    }

    public function actionApprove($order_id)
    {
        $model = $this->findModel($order_id);
        $model->labsToArray();
        $model->xraysToArray();
        $model->approve_order_id = Yii::$app->user->identity->id;
        $model->save();

        return $this->redirect(['list', 'id' => $model->an]);
    }

    /**
     * Displays a single DoctorOrders model.
     * @param int $order_id Order ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($order_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($order_id),
        ]);
    }

    /**
     * Creates a new DoctorOrders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($an)
    {
        $admit = Ipt::find()->where(['an' => $an])->one();

        $model = new DoctorOrders();
        $last_order = DoctorOrders::find()->where(['an' => $an])->orderBy(['order_id' => SORT_DESC])->one();
        $total = DoctorOrders::find()->where(['an' => $an])->count();
        $model->an = $an;
        $model->user_id = Yii::$app->user->identity->id;
        $model->ward = $admit->ward;
        $model->bed_no = $admit->iptadm->getBedName();
        $model->order_datetime = date('Y-m-d H:i:s');
        $model->specialist_progress_note = [];

        if ($last_order) {
            $model->food = $last_order->food;
            $model->record_io = $last_order->record_io;
            $model->record_vitalsign = $last_order->record_vitalsign;
        }

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                if ($model->save()) {
                    if (Yii::$app->user->identity->role == 4) {
                        return $this->redirect(['list-order', 'id' => $model->ward]);
                    }
                    if (Yii::$app->user->identity->role == 5) {
                        return $this->redirect(['list', 'id' => $model->an]);
                    }
                    return $this->redirect(['list', 'id' => $model->an]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'admit' => $admit,
            'total' => $total
        ]);
    }

    /**
     * Updates an existing DoctorOrders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $order_id Order ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($order_id)
    {
        $model = $this->findModel($order_id);
        $model->labsToArray();
        $model->xraysToArray();
        
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['list', 'id' => $model->an]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Lists all Orders where AN .
     * @return mixed
     */
    public function actionPrint($id)
    {
        $model = DoctorOrders::find()->where(['an' => $id])->all();
        $pt = Ipt::find()->where(['an' => $id])->one();
        $total = DoctorOrders::find()->where(['an' => $id])->count();
        $office = HiSetup::find()->where(['common_code' => 'hosp_info'])->one();

        $content = $this->renderPartial('_print', [
            'model' => $model,
            'pt' => $pt,
            'total' => $total,
            'office' => $office,
        ]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Doctor Order : ' . $id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }

    /**
     * Lists all Orders where AN .
     * @return mixed
     */
    public function actionOrder($id)
    {
        $order = $this->findModel($id);
        $pt = Ipt::find()->where(['an' => $order->an])->one();
        $office = HiSetup::find()->where(['common_code' => 'hosp_info'])->one();

        $content = $this->renderPartial('_order', [
            'order' => $order,
            'pt' => $pt,
            'office' => $office,
        ]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Doctor Order : ' . $id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }

    public function actionSwitchsort($id)
    {
        $layout = HiSetup::find()->where(['common_code' => 'order_sort'])->one();
        if ($layout->value1 == 'asc') {
            $layout->value1 = 'desc';
            $layout->save();
        } else {
            $layout->value1 = 'asc';
            $layout->save();
        }
        return $this->redirect(['list', 'id' => $id]);
    }
    /**
     * Deletes an existing DoctorOrders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $order_id Order ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($order_id)
    {
        $this->findModel($order_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DoctorOrders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $order_id Order ID
     * @return DoctorOrders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($order_id)
    {
        if (($model = DoctorOrders::findOne(['order_id' => $order_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
