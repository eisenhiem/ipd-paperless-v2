<?php

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LStandingOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Standing Orders';
?>
<div class="lstanding-order-index">


    <p>
        <?= Html::a('เพิ่ม Standing Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' =>[
            'before'=>' '
        ],
    //  'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'standing_order_id',
            'standing_order_name',
            'user_id',
            'standing_order_type',
            'standing_order_map_code',
            'medusage',
            'qty',
            [
                'header' => 'สถานะ',
                'value' => function ($model) {
                    if ($model->is_active == '1') {
                        return Html::a('Active', ['change', 'standing_order_id' => $model->standing_order_id], [
                            'class' => 'btn btn-xs btn-success btn-block',
                            'data-method' => 'post',
                        ]);
                    } else {
                        return Html::a('Disable', ['change', 'standing_order_id' => $model->standing_order_id], [
                            'class' => 'btn btn-xs btn-danger btn-block',
                            'data-method' => 'post',
                        ]);
                    }
                },
                'format' => 'raw',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'แก้ไข',
                'options'=>['style'=>'width:60px;'],
                'template'=>'{update}',
                'buttons'=>[
                  'update' => function($url,$model,$key){
                    return Html::a(Icon::show('edit'), ['update','standing_order_id'=>$model->standing_order_id], ['class' => 'btn btn-warning','style' =>['width'=>'60px']]) ;
                  },
                ]
            ],

        ],
    ]); ?>


</div>
