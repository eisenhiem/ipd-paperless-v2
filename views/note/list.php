<?php

use app\models\Ipt;
use app\models\VsDaily;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AdmissionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nurse Note AN:' . $an . ' ชื่อ-สกุล:' . $ward->pt->getFullName();
?>
<div class="admission-index">

    <p>
        <?= Html::a(Icon::show('chevron-left') . 'Home', ['admission/index', 'ward_id' => $ward->ward], ['class' => 'btn btn-danger']) ?>
        <?= in_array(Yii::$app->user->identity->role,[3,9]) ? Html::a(Icon::show('plus') . 'เพิ่ม Note', ['create', 'an' => $an], ['class' => 'btn btn-success']) : '' ?>
        <?= in_array(Yii::$app->user->identity->role,[3,9]) && $order->value1 == 'asc' ? Html::a(Icon::show('sort-amount-down') . 'เรียง Z > A', ['switchsort', 'id' => $an], ['class' => 'btn btn-primary']) : Html::a(Icon::show('sort-amount-down-alt') . 'เรียง A > Z', ['switchsort', 'id' => $an], ['class' => 'btn btn-primary']); ?>
        <?= in_array(Yii::$app->user->identity->role,[3,9]) ? Html::a(Icon::show('flask') . 'Lab', ['admission/lab', 'id' => $an], ['class' => 'btn btn-warning']):'' ?>
        <?= Html::a(Icon::show('thermometer-half') . 'V/S', ['vitalsign', 'id' => $an], ['class' => 'btn btn-info']) ?>
        <?= in_array(Yii::$app->user->identity->role,[1,3,9]) ? Html::a(Icon::show('plus') . Icon::show('thermometer-half') . 'V/S', ['add-vs', 'an' => $an], ['class' => 'btn btn-success']) : '' ?>
        <?= in_array(Yii::$app->user->identity->role,[3,5,6,7,8,9]) ? Html::a(Icon::show('notes-medical') . 'Doctor Order ', ['order/list', 'id' => $an], ['class' => 'btn btn-primary']):'' ?>
        <?= Html::a(Icon::show('print') . ' พิมพ์', ['print', 'id' => $an], ['class' => 'btn btn-warning', 'target' => '_blank']) ?>
    </p>


    <table width="100%" border="1">
        <tr>
            <th width="15%" style="text-align:center">Date/Time</th>
            <th width="20%" style="text-align:center">Problem</th>
            <th width="30%" style="text-align:center">Activity</th>
            <th width="35%" style="text-align:center">Evaluate</th>
        </tr>
        <?php
        if ($model) {
            foreach ($model as $note) {
        ?>
                <tr>
                    <td style="vertical-align:top;text-align:center">
                        <?= Ipt::getThaiDate($note->note_date) ?><br>
                        <?= $note->note_time ?> น.<br>
                    </td>
                    <td style="vertical-align:top;padding:5px;">
                        <?= nl2br($note->problem) ?>
                    </td>
                    <td style="vertical-align:top;padding:5px;">
                        <?= $note->activity_select ? $note->getActName(): '' ?>
                        <?= nl2br($note->activity) ?>
                    </td>
                    <td style="vertical-align:top;padding:5px;">
                        <?= $note->evaluate_select ? $note->getEvaname():'' ?>
                        <?= nl2br($note->evaluate) ?>
                        <br><br>
                        <b>ผู้ประเมิน</b>
                        &emsp; <?= $note->profile->fullname ?><br>
                        &emsp; <?= $note->profile->position ?><br>
                        &emsp;<?= Yii::$app->user->identity->id == $note->user_id && $note->note_date == date('Y-m-d') ? Html::a('แก้ไขบันทึก', ['update', 'note_id' => $note->note_id], ['class' => 'btn btn-warning']) : '' ?><br>
                    </td>
                </tr>
        <?php
            }
        }
        ?>
    </table>

</div>