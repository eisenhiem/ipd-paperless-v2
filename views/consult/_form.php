<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IptConsult */
/* @var $form yii\widgets\ActiveForm */

$consult = ['กายภาพ' => 'กายภาพ', 'เวชปฏิบัติฯ' => 'เวชปฏิบัติฯ', 'เภสัช' => 'เภสัช', 'ทันตกรรม' => 'ทันตกรรม', 'แพทย์เฉพาะทาง' => 'แพทย์เฉพาะทาง'];

?>

<div class="ipt-consult-form">

  <?php $form = ActiveForm::begin(); ?>
  <div class="card bg-primary">
    <div class="card-header">
      <?= $form->field($model, 'consult_type')->radioList($consult) ?>
    </div>
    <div class="card-body bg-light">
      <?= $form->field($model, 'consult_description')->textarea(['rows' => 6]) ?>
    </div>
    <div class="card-footer bg-light">
      <div class="form-group" style="text-align: center;">
        <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success', 'style' => 'width:200px']) ?>
        &emsp;
        <?= Html::a(Icon::show('chevron-left') . ' กลับไปหน้าหลัก', ['order/list', 'id' => $an], ['class' => 'btn btn-danger', 'style' => 'width:200px']) ?>
      </div>
    </div>

    <?php ActiveForm::end(); ?>

  </div>