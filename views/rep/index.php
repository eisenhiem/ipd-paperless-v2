<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ConsultRepSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consult Reps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consult-rep-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Consult Rep', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'rep_id',
            'consult_id',
            'order_id',
            'user_id',
            'rep_detail:ntext',
            //'rep_datetime',
            //'d_update',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, ConsultRep $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'rep_id' => $model->rep_id]);
                 }
            ],
        ],
    ]); ?>


</div>
