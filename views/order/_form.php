<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Lab;
use inquid\signature\SignatureWidgetInput;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\LProgressNote;

$getlab = ArrayHelper::map(Lab::find()->all(), 'labcode', 'labname');

$standing = LProgressNote::find()->where(['specialist_group_code' => Yii::$app->user->identity->profile->specialist, 'is_active' => '1'])->count();

if($standing < 15) {
    $row = 30;
    $oneday_row = 15;
} else {
    $row = $standing + 10;
}

$s = ArrayHelper::map(LProgressNote::find()->where(['specialist_group_code' => Yii::$app->user->identity->profile->specialist, 'is_active' => '1', 'progress_type' => 'S'])->all(), 'progress_value', 'progress_value');
$o = ArrayHelper::map(LProgressNote::find()->where(['specialist_group_code' => Yii::$app->user->identity->profile->specialist, 'is_active' => '1', 'progress_type' => 'O'])->all(), 'progress_value', 'progress_value');
$a = ArrayHelper::map(LProgressNote::find()->where(['specialist_group_code' => Yii::$app->user->identity->profile->specialist, 'is_active' => '1', 'progress_type' => 'A'])->all(), 'progress_value', 'progress_value');
$p = ArrayHelper::map(LProgressNote::find()->where(['specialist_group_code' => Yii::$app->user->identity->profile->specialist, 'is_active' => '1', 'progress_type' => 'P'])->all(), 'progress_value', 'progress_value');    

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-9">
            <div class="card">
                <div class="card-header bg-primary text-center">
                    <h5 class="mb-0">
                        <?= Icon::show('fish') ?> อาหาร
                    </h5>
                </div>
                <div class="card-body">
                    <?= $form->field($model, 'food')->radioList($model->getItemFood())->label(false) ?>
                    <?= $form->field($model, 'extra_food')->textInput(['placeholder' => 'อื่นๆ ระบุ'])->label(false) ?>

                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-header bg-info text-center">
                    <h5 class="mb-0">
                        <?= Icon::show('clipboard-list') ?> Record
                    </h5>
                </div>
                <div class="card-body">
                    <?= $form->field($model, 'record_io')->checkbox() ?>
                    <?= $form->field($model, 'record_vitalsign')->checkbox() ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-deck">
                <div class="card">
                    <div class="card-header text-center" style="background:#47B5FF">
                        <h5 class="mb-0">
                            <?= Icon::show('notes-medical') ?> Progress Note
                        </h5>
                    </div>
                    <div class="card-body">
                        <?php 
                            if($s){
                                echo $form->field($model, 'subjective')->checkboxList($s,['item' => function ($index, $label, $name, $checked, $value) {
                                return "<label class='col-12'><input type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label} </label>";
                                }])->label('Subjective');
                            }
                       ?>
                        <?php 
                            if($o){
                                echo $form->field($model, 'objective')->checkboxList($o,['item' => function ($index, $label, $name, $checked, $value) {
                                return "<label class='col-12'><input type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label} </label>";
                                }])->label('Objective');
                            }
                        ?>
                        <?php 
                            if($s){
                                echo $form->field($model, 'assessment')->checkboxList($a,['item' => function ($index, $label, $name, $checked, $value) {
                                return "<label class='col-12'><input type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label} </label>";
                                }])->label('Assessment'); 
                            }
                        ?>
                        <?php 
                            if($p){
                                echo $form->field($model, 'plan')->checkboxList($p,['item' => function ($index, $label, $name, $checked, $value) {
                                return "<label class='col-12'><input type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label} </label>";
                            }])->label('Plan');
                            } 
                        ?>
                        <?php 
                            if($standing > 0) { 
                                echo $form->field($model, 'progress_note')->textarea(['rows' => 10])->label(false);
                            } else {
                                echo $form->field($model, 'progress_note')->textarea(['rows' => $row])->label(false);
                            } 
                        ?>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header bg-success text-center">
                        <h5 class="mb-0">
                            <?= Icon::show('list') ?> Order One Day
                        </h5>
                    </div>
                    <div class="card-body">
                        <?= $form->field($model, 'labs')->checkboxList($model->getItemlabs(),['item' => function ($index, $label, $name, $checked, $value) {
                                return "<label class='col-12'><input type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label} </label>";
                            }])->label('Standing Order') ?>
                        <?= $form->field($model, 'order_oneday')->textarea(['rows' => $oneday_row])->label('ยาและอื่น ๆ เพิ่มเติม') ?>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header bg-primary text-center">
                        <h5 class="mb-0">
                            <?= Icon::show('list') ?> Order Continue
                        </h5>
                    </div>
                    <div class="card-body">
                        <?= $form->field($model, 'order_continue')->textarea(['rows' => $row])->label(false) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12">
            <?= $form->field($model, 'order_comment')->textarea(['rows' => 2]) ?>
        </div>
    </div>
    <table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
        <tr>
            <td valign="top"><b>หมายเหตุ</b></td>
            <td>1.ให้มีการบันทึก Progress Note ใน 3 วันแรกของการรักษานรูปแบบ SOAP หากไม่เขียน SOAP กำกับ ขอให้บันทึกในรูปแบบ SOAP<br>
                2.ให้มีการบันทึกทุกครั้งที่มีการเปลี่ยนแปลงแพทย์ที่ดูแล การรักษา การให้ยา การทำ Invasive Procedure และการประเมินผลและแปรผล<br>
                3.บันทึกที่ลายมือที่อ่านออกได้ง่าย และเซ็นชื่อกำกับทุกครั้งโดยให้สามารถระบุได้ว่าเป็นแพทย์ท่านใด<br>
                4.พยาบาลลงลายมือชื่อและตำแหน่งที่สามารถอ่านออกได้ทุกครั้งขณะลงนามในแบบฟอร์มการตรวจของแพทย์พร้อมระบุนเวลาที่รับคำสั่ง
            </td>
        </tr>
    </table>
    <br>
    <div class="form-group">
        <?= Html::submitButton(Icon::show('save') . 'บันทึก Order', ['class' => 'btn btn-success', 'style' => 'width:200px']) ?>
        &emsp;
        <?= Html::a(Icon::show('chevron-left') . 'กลับไปหน้าหลัก', ['list', 'id' => $model->an], ['class' => 'btn btn-danger', 'style' => 'width:200px']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>