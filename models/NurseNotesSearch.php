<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NurseNotes;

/**
 * NurseNotesSearch represents the model behind the search form of `app\models\NurseNotes`.
 */
class NurseNotesSearch extends NurseNotes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['note_id', 'an','user_id'], 'integer'],
            [['note_date', 'note_time', 'activity', 'evaluate', 'problem', 'd_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NurseNotes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'note_id' => $this->note_id,
            'an' => $this->an,
            'note_date' => $this->note_date,
            'user_id' => $this->user_id,
            'd_update' => $this->d_update,
        ]);

        $query->andFilterWhere(['like', 'note_time', $this->note_time])
            ->andFilterWhere(['like', 'activity', $this->activity])
            ->andFilterWhere(['like', 'evaluate', $this->evaluate]);

        return $dataProvider;
    }
}
