<?php

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LProgressNoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Progress Note';

?>
<div class="lprogress-note-index">

    <p>
        <?= Html::a('เพิ่ม Standing Progress Note', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' =>[
            'before'=>' '
        ],
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'progress_id',
            'specialist_group_code',
            'progress_type',
            'progress_value',
            [
                'header' => 'สถานะ',
                'value' => function ($model) {
                    if ($model->is_active == '1') {
                        return Html::a('Active', ['change', 'progress_id' => $model->progress_id], [
                            'class' => 'btn btn-xs btn-success btn-block',
                            'data-method' => 'post',
                        ]);
                    } else {
                        return Html::a('Disable', ['change', 'progress_id' => $model->progress_id], [
                            'class' => 'btn btn-xs btn-danger btn-block',
                            'data-method' => 'post',
                        ]);
                    }
                },
                'format' => 'raw',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'แก้ไข',
                'options'=>['style'=>'width:60px;'],
                'template'=>'{update}',
                'buttons'=>[
                  'update' => function($url,$model,$key){
                    return Html::a(Icon::show('edit'), ['update','progress_id'=>$model->progress_id], ['class' => 'btn btn-warning','style' =>['width'=>'60px']]) ;
                  },
                ]
            ],

        ],
    ]); ?>


</div>
