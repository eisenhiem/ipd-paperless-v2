<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IptOwnerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'แพทย์เจ้าของไข้';

?>
<div class="ipt-owner-index">

    <p>
        <?= Html::a(Icon::show('plus').'เพิ่มแพทย์', ['create','an'=>$an,'ward' => $ward], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'header' => 'ประเภท',
                'value' => function($model){
                    return $model->getTypeName();
                }
            ],
            [
                'header' => 'ชื่อ-สกุล แพทย์',
                'value' => function($model){
                    return $model->profile->fullname;
                }
            ],
            [
                'header' => 'สถานะ',
                'value' => function ($model) {
                    if ($model->owner_status == '1') {
                        return Html::a('Active', ['change', 'id' => $model->owner_id], [
                            'class' => 'btn btn-xs btn-success btn-block',
                            'data-method' => 'post',
                        ]);
                    } else {
                        return Html::a('Disable', ['change', 'id' => $model->owner_id], [
                            'class' => 'btn btn-xs btn-danger btn-block',
                            'data-method' => 'post',
                        ]);
                    }
                },
                'format' => 'raw',
            ],
        ],
    ]); ?>


</div>
