<?php

use app\models\DoctorOrders;
use app\models\Idpm;
use app\models\Ipt;

$ward = Idpm::find()->all();
$pt = Ipt::find()->where(['rgtdate'=>date('y-m-d')])->andWhere(['ward' => ''])->count();

$r = [];
if($pt > 0){
    $i = ['label' => 'ยังไม่ระบุ', 'url' => ['admission/index','ward_id'=> '' ], 'iconStyle' => 'far', 'icon' => 'dot-circle','badge' => '<span class="right badge badge-danger">'.$pt.'</span>'];
} else {
    $i = ['label' => 'ยังไม่ระบุ', 'url' => ['admission/index','ward_id'=> '' ], 'iconStyle' => 'far', 'icon' => 'dot-circle'];
}

if(Yii::$app->user->identity->role == 2) {
    $consult = ['label' => 'เวชปฏิบัติฯ', 'url' => ['/consult/index','type'=>'เวชปฏิบัติฯ'], 'icon' => 'users',];
}
if(Yii::$app->user->identity->role == 4) {
    $consult = ['label' => 'เภสัชกร', 'url' => ['/consult/index','type'=>'เภสัช'], 'icon' => 'users',];
}
if(Yii::$app->user->identity->role == 6) {
    $consult = ['label' => 'กายภาพ', 'url' => ['/consult/index','type'=>'กายภาพ'], 'icon' => 'users',];
}
if(Yii::$app->user->identity->role == 7) {
    $consult = ['label' => 'ทันตะ', 'url' => ['/consult/index','type'=>'ทันตกรรม'], 'icon' => 'users',];
}
if(Yii::$app->user->identity->role == 8) {
    $consult = ['label' => 'แพทย์เฉพาะทาง', 'url' => ['/consult/index','type'=>'แพทย์เฉพาะทาง'], 'icon' => 'users',];
}

array_push($r,$i);
foreach($ward as $w){
    $pt = Ipt::find()->where(['ward'=>$w->idpm,'dchdate'=> '0000-00-00'])->count();
    $order = DoctorOrders::find()->where(['<>','order_status','Complete'])->andWhere(['ward'=>$w->idpm,])->count();
    if(Yii::$app->user->identity->role == 4 ){
        if($order > 0){
            $i = ['label' => $w->nameidpm , 'url' => ['order/list-order','ward_id'=>$w->idpm], 'iconStyle' => 'far', 'icon' => 'dot-circle','badge' => '<span class="right badge badge-danger">'.$order.'</span>'];
        } else {
            $i = ['label' => $w->nameidpm , 'url' => ['order/list-order','ward_id'=>$w->idpm], 'iconStyle' => 'far', 'icon' => 'dot-circle'];
        }    
    } else {
        if($pt > 0){
            $i = ['label' => $w->nameidpm , 'url' => ['admission/index','ward_id'=>$w->idpm], 'iconStyle' => 'far', 'icon' => 'dot-circle','badge' => '<span class="right badge badge-warning">'.$pt.'</span>'];
        } else {
            $i = ['label' => $w->nameidpm , 'url' => ['admission/index','ward_id'=>$w->idpm], 'iconStyle' => 'far', 'icon' => 'dot-circle'];
        }    
    }
    array_push($r,$i);
}

?>



?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?=\yii\helpers\Url::home()?>" class="brand-link">
        <img src="<?= Yii::getAlias('@web') ?>/images/logo.png" alt="IPD  Paperless Logo" class="brand-image img-square elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">IPD Paperless</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <!-- SidebarSearch Form -->
        <!-- href be escaped -->
        <!-- <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div> -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <?php
                  $admit = Ipt::find()->where(['dchdate'=> '0000-00-00'])->count();
                  $orders = DoctorOrders::find()->where(['<>','order_status','Complete'])->count();
                  if(Yii::$app->user->identity->role == 4 ){
                    $admit = $orders;
                  }
            echo \hail812\adminlte\widgets\Menu::widget([
                'items' => [
                    [
                        'label' => 'Ward',
                        'icon' => 'building',
                        'badge' => '<span class="right badge badge-warning">'.$admit.'</span>',
                        'items' => $r,
                    ],
                    [
                        'label' => 'Consult',
                        'visible' => in_array(Yii::$app->user->identity->role, [3,4,6,7,8]),
                        'items' => [$consult],
                    ],
                    ['label' => 'Admin Menu', 'header' => true],
                    [
                        'label' => 'ตั้งค่า',
                        'icon' => 'cogs',
//                        'badge' => '<span class="right badge badge-info">2</span>',
                        'visible' => Yii::$app->user->identity->role == 9,
                        'items' => [
                            ['label' => 'ตั้งค่าผู้ใช้', 'url' => ['/user/admin/index'], 'icon' => 'users',],
                            ['label' => 'ตั้งค่ากิจกรรม', 'url' => ['/activity/index'], 'icon' => 'user-nurse'],
                            ['label' => 'ตั้งค่าประเมิน', 'url' => ['/evaluate/index'], 'icon' => 'sliders-h'],
                            ['label' => 'ตั้งค่า Progress Note', 'url' => ['/progress-note/index'], 'icon' => 'sliders-h'],
                            ['label' => 'ตั้งค่า Standing Order', 'url' => ['/standing-order/index'], 'icon' => 'sliders-h'],
//                            ['label' => 'Gii',  'icon' => 'file-code', 'url' => ['/gii'], 'target' => '_blank', 'iconClassAdded' => 'text-warning'],
                        ]
                    ],
                    ['label' => 'Login', 'url' => ['/user/security/login'], 'icon' => 'sign-in-alt', 'visible' => Yii::$app->user->isGuest],
                    ['label' => 'Logout', 'url' => ['/user/security/logout'], 'icon' => 'sign-out-alt', 'visible' => !Yii::$app->user->isGuest, 'template' => '&emsp;<a href="{url}" data-method="post">{icon}</a>'],
                ],
            ]);
            ?>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>