<?php

use app\models\Labresult;
use app\models\Lbbk;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ipt */

$this->title = 'AN: '.$model->an;
\yii\web\YiiAsset::register($this);
?>
<div class="ipt-view">
  <div class="row">
    <div class="col-10 offset-2">
    <?= Html::a(Icon::show('chevron-left').'กลับ', ['index','ward_id' => $ward->ward], ['class' => 'btn btn-danger']) ?>
    &emsp; <?= $ward->dchdate == '0000-00-00' ? Html::a(Icon::show('plus').'เพิ่ม Order', ['order/create','an'=>$model->an], ['class' => 'btn btn-success']):'' ?>
    &emsp; <?= Html::a(Icon::show('flask').'Lab', ['lab','id'=>$model->an], ['class' => 'btn btn-primary']) ?>
    &emsp; <?= Html::a(Icon::show('clipboard').'Doctor Order', ['order/list','id'=>$model->an], ['class' => 'btn btn-info']) ?>
    &emsp; <?= Html::a(Icon::show('notes-medical').'Nurse Note', ['note/list','id'=>$model->an], ['class' => 'btn btn-warning']) ?>
    </div>
  </div>
  <br>
<div class="card text-white bg-primary">
  <div class="card-header">ข้อมูลทั่วไป</div>
  <div class="card-body bg-white">
    <p class="card-text">
      &emsp;<b>HN: <?php echo $model->hn; ?></b>
      &emsp;<b>AN: <?php echo $model->an; ?></b>
      &emsp;<b>ชื่อ-สกุล: </b><?php echo $model->prename . $model->ptname; ?>
      &emsp;<b>อายุ </b><?php echo $model->age; ?> ปี
      &emsp;<b>เพศ </b><?php echo $ptinfo->sex; ?>
    </p>
  </div>
</div>
<div class="card-group">
<div class="card text-white bg-info">
  <div class="card-header">ประวัติ</div>
  <div class="card-body bg-white">
    <p class="card-text">
      &emsp;<b>Chief Compaint: </b>
      <?= $model->cc; ?><br>
      &emsp;<b>Present Illness: </b>
      <?= $model->pi; ?><br>
      &emsp;<b>Past History: </b>
      <?= $model->ph ?><br>
      &emsp;<b>ประวัติการแพ้ยา: </b>
      &emsp;<?= $allergy ? $allergy->drug:'-' ?><br>
      &emsp;<b>การตั้งครรภ์: </b>
      <?= $visit->preg == '1' ? 'ตั้งครรภ์':'-' ?><br>
    </p>
  </div>
</div>
<div class="card text-white bg-success">
  <div class="card-header">การรับรักษา</div>
  <div class="card-body bg-white">
    <p class="card-text">
      <h3>Previous Diagnosis : </h3>
      &emsp;<?php echo str_replace(', unspecified','',$model->prediag); ?><br>
      <h3>Vital Sign</h3>
      &emsp;<b>BT:</b> <?php echo $model->tt; ?> ºC
      <b>BP:</b> <?php echo $model->sbp . '/' . $model->dbp; ?> mmHg
      <b>PR:</b> <?php echo $model->pr ?> /m
      <b>RR:</b> <?php echo $model->rr ?> /m
      <b>น้ำหนัก:</b> <?php echo $model->bw ?> kg
      <b>ส่วนสูง:</b> <?php echo $model->height ?> cm <br><br>
      <h3>ผลการตรวจร่างกาย</h3>
      &emsp;<b>PE:</b> <?php echo $model->pe ?><br>
    </p>
  </div>
</div>
</div>
