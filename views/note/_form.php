<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Rounds;
use kartik\date\DatePicker;
use kartik\icons\Icon;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Notes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notes-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card bg-primary">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <?= $form->field($model, 'note_date')->widget(
                        DatePicker::ClassName(),
                        [
                            'name' => 'วันที่บันทึก',
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'options' => ['placeholder' => 'ระบุวัน'],
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true
                            ]
                        ]
                    )->label(false);
                    ?>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6">
                    <?= $form->field($model, 'note_time')->widget(
                        MaskedInput::className(),
                        [
                            'mask' => '99:99'
                        ]
                    )->label(false);
                    ?>
                </div>
            </div>
        </div>
        <div class="card-body bg-light">
            <div class="row">
                <div class="col-12">
                    <?= $form->field($model, 'problem')->textarea(['rows' => 3, 'placeholder' => 'บันทึกปัญหาที่พบในการดูแลผู้ป่วย ...'])->label('ปัญหา (Problem)') ?>
                </div>
                <div class="col-12">
                    <?= $form->field($model, 'activity_select')->checkboxList($model->getActUse(), ['item' => function ($index, $label, $name, $checked, $value) {
                        return "<label class='col-12'><input type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label} </label>";
                    }])->label('กิจกรรมทางการพยาบาล (Nurse Activity)') ?>
                </div>
                <div class="col-12">
                    <?= $form->field($model, 'activity')->textarea(['rows' => 3, 'placeholder' => 'บันทึกกิจกรรมการดูแลผู้ป่วย ...'])->label('กิจกรรมเพิ่มเติม') ?>
                </div>
                <div class="col-12">
                    <?= $form->field($model, 'evaluate_select')->checkboxList($model->getEvaUse(), ['item' => function ($index, $label, $name, $checked, $value) {
                        return "<label class='col-12'><input type='checkbox' {$checked} name='{$name}' value='{$value}'> {$label} </label>";
                    }])->label('การประเมินผู้ป่วยหลังทำกิจกรรมทางการพยาบาล (Nurse Evaluate)') ?>
                </div>
                <div class="col-12">
                    <?= $form->field($model, 'evaluate')->textarea(['rows' => 3, 'placeholder' => 'บันทึกการประเมินผู้ป่วยหลังทำกิจกรรม ...'])->label('การประเมินเพิ่มเติม') ?>
                </div>
            </div>
        </div>
        <div class="card-footer bg-light">
            <div class="form-group" style="text-align: center;">
                <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success', 'style' => 'width:200px']) ?>
                &emsp;
                <?= Html::a(Icon::show('chevron-left') . ' กลับไปหน้าหลัก', ['list', 'id' => $model->an], ['class' => 'btn btn-danger', 'style' => 'width:200px']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>