<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;

?>

<?php 
/*
echo Highcharts::widget([
    'options' => [
        'chart' => [
             'type' => 'line',
        ],
       'title' => ['text' => 'V/S'],
       'xAxis' => [
           'title' => 'วันเวลา',
          'categories' => $date
       ],
       'yAxis' => [
          [ // primary axis
              'title' => ['text' => 'temp  ( ํc)'],
              'min' => 35,
              'max' => 42,
         ],
         [ // secondary axis
             'gridLineWidth' => 0,
             'title' => ['text' => 'Pluse Rate /min'],
             'max' => 160,
             'min' => 40,
             'opposite' => true,
         ]
         ],
       'series' => [
          [
             'yAxis' => 0 ,
             'name' => 'BT',
             'data' => $bt,
             'style' => [
                 'color' => 'red',
             ],
         ],
         [
             'yAxis' => 1 ,
             'name' => 'pluse rate',
             'data' => $pr,            
             'style' => [
                 'color' => 'blue',
             ],
         ],
          ]
    ]
 ]);
 */
 ?>

 <table width="100%" border=1>
<tr>
    <th>Date</th>
<?php 
    foreach($date as $d) {
        echo '<th>'.$d.'</th>';
    }
?>
</tr>
<tr>
    <td>RR</td>
<?php 
    foreach($rr as $d) {
        if($d == 0){
            $d='N/A';
        }
        echo '<td>'.$d.'</td>';
    }
?>
</tr>
<tr>
    <td>SBP</td>
<?php 
    foreach($sbp as $d) {
        if($d == 0){
            $d='N/A';
        }
        echo '<td>'.$d.'</td>';
    }
?>
</tr>
<tr>
    <td>DBP</td>
<?php 
    foreach($dbp as $d) {
        if($d == 0){
            $d='N/A';
        }
        echo '<td>'.$d.'</td>';
    }
?>
</tr>
<tr>
    <td>In Take</td>
<?php 
    foreach($intake as $d) {
        if($d == 0){
            $d='N/A';
        }
        echo '<td>'.$d.'</td>';
    }
?>
</tr>
<tr>
    <td>Out Take</td>
<?php 
    foreach($outtake as $d) {
        if($d == 0){
            $d='N/A';
        }
        echo '<td>'.$d.'</td>';
    }
?>
</tr>
<tr>
    <td>Stools</td>
<?php 
    foreach($stool as $d) {
        if($d == 0){
            $d='N/A';
        }
        echo '<td>'.$d.'</td>';
    }
?>
</tr>
<tr>
    <td>Urines</td>
<?php 
    foreach($urine as $d) {
        if($d == 0){
            $d='N/A';
        }
        echo '<td>'.$d.'</td>';
    }
?>
</tr>

 </table>