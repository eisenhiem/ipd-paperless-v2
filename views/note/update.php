<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NurseNotes */

$this->title = 'Update Nurse Notes: ' . $model->note_id;
?>
<div class="nurse-notes-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
