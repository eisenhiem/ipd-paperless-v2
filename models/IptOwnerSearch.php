<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IptOwner;

/**
 * IptOwnerSearch represents the model behind the search form of `app\models\IptOwner`.
 */
class IptOwnerSearch extends IptOwner
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['owner_id', 'an', 'owner_user_id'], 'integer'],
            [['owner_type', 'owner_status', 'd_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IptOwner::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'owner_id' => $this->owner_id,
            'an' => $this->an,
            'owner_user_id' => $this->owner_user_id,
            'd_update' => $this->d_update,
        ]);

        $query->andFilterWhere(['like', 'owner_type', $this->owner_type])
            ->andFilterWhere(['like', 'owner_status', $this->owner_status]);

        return $dataProvider;
    }
}
