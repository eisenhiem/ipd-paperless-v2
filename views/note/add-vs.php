<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NurseNotes */

$this->title = 'บันทึก Vital Sign';
?>
<div class="vital-sign-create">

    <?= $this->render('_form_vs', [
        'model' => $model,
    ]) ?>

</div>