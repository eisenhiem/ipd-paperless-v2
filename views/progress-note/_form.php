<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Profile;

/* @var $this yii\web\View */
/* @var $model app\models\LProgressNote */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lprogress-note-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-8 offset-2 col-sm-10 offset-1">
            <div class="card border-primary">
                <div class="card-header" style="background-color:cornflowerblue;color:white">
                    <?= $form->field($model, 'specialist_group_code')->dropDownList(Profile::getItemSpclty()) ?>
                </div>
                <div class="card-body">

                    <?= $form->field($model, 'progress_type')->radioList(['S' => 'Subjective ', 'O' => 'Objective', 'A' => 'Assessment','P' => 'Plan', ], ['prompt' => '']) ?>

                    <?= $form->field($model, 'progress_value')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="card-footer">
                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
