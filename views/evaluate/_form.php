<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LEvaluate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="levaluate-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'evaluate_id')->textInput(['maxlength' => true,'placeholder' => 'ใส่ได้สองตัวอักษร ex. 01']) ?>

    <?= $form->field($model, 'evaluate_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
