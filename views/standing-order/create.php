<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LStandingOrder */

$this->title = 'เพิ่ม Standing Order';

?>
<div class="lstanding-order-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
