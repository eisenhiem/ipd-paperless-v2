<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prcd".
 *
 * @property int $id
 * @property string $nameprcd
 * @property string $codeprcd
 * @property string $fullname
 * @property int $priceprcd
 * @property string $income
 * @property string $cgd
 * @property string $etype
 * @property string $ptright
 * @property int $pricecgd
 * @property int $costprcd
 */
class Prcd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prcd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nameprcd', 'codeprcd', 'fullname', 'priceprcd', 'income', 'cgd', 'etype', 'ptright', 'pricecgd'], 'required'],
            [['priceprcd', 'pricecgd', 'costprcd'], 'integer'],
            [['nameprcd', 'fullname'], 'string', 'max' => 90],
            [['codeprcd'], 'string', 'max' => 7],
            [['income'], 'string', 'max' => 2],
            [['cgd'], 'string', 'max' => 5],
            [['etype'], 'string', 'max' => 100],
            [['ptright'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nameprcd' => 'Nameprcd',
            'codeprcd' => 'Codeprcd',
            'fullname' => 'Fullname',
            'priceprcd' => 'Priceprcd',
            'income' => 'Income',
            'cgd' => 'Cgd',
            'etype' => 'Etype',
            'ptright' => 'Ptright',
            'pricecgd' => 'Pricecgd',
            'costprcd' => 'Costprcd',
        ];
    }
}
