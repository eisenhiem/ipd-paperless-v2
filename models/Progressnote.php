<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "progressnote".
 *
 * @property int $id
 * @property int $an
 * @property string $vstdate
 * @property string $dct
 * @property int $vsttime
 * @property string $prognote
 * @property string $d_update
 */
class Progressnote extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'progressnote';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'vstdate', 'dct', 'vsttime', 'prognote', 'd_update'], 'required'],
            [['an', 'vsttime'], 'integer'],
            [['vstdate', 'd_update'], 'safe'],
            [['prognote'], 'string'],
            [['dct'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'an' => 'An',
            'vstdate' => 'Vstdate',
            'dct' => 'Dct',
            'vsttime' => 'Vsttime',
            'prognote' => 'Prognote',
            'd_update' => 'D Update',
        ];
    }
}
