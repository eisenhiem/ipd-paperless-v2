<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DoctorOrdersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="doctor-orders-search">

    <?php $form = ActiveForm::begin([
        'action' => ['list-order', 'ward_id' => $ward_id],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-sx-8">
            <?= $form->field($model, 'an')->textInput(['placeholder' => 'ระบุ AN'])->label(false) ?>
        </div>
        <div class="col-2">
            <div class="form-group">
                <?= Html::submitButton(Icon::show('search'), ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>