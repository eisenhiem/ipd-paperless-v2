<?php

use app\models\Labresult;
?>
<table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
    <tr>
        <th colspan="3">Lab: <b><?= $lab->labname ?></b></th>
    </tr>
    <tr>
        <td width="50%"><b>&emsp; ชื่อ-สกุล </b><br>&emsp; &emsp; &emsp;<?= $pt->pt->getFullName() ?></td>
        <td width="25%">&emsp; <b>อายุ</b> <?= $pt->pt->getAge() ?><br>&emsp; <b>เพศ </b> <?= $pt->pt->getGender() ?></td>
        <td width="25%">&emsp; <b>HN:</b> <?= $pt->hn ?><br>&emsp; <b>AN:</b> <?= $pt->an ?></td>
    </tr>
</table>
<table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
    <?php
    foreach ($model as $r) {
        $result = Labresult::find()->joinWith('label')->where(['ln' => $r->ln])->orderBy(['id' => SORT_DESC])->all();
        $row = Labresult::find()->joinWith('label')->where(['ln' => $r->ln])->orderBy(['id' => SORT_DESC])->count();
        echo '<tr><td colspan=2 align="center">วันที่' . $r->getServdate() . '</td></tr>';
        $i = 0;
        foreach ($result as $v) {
            if($i % 2 == 0 || $i == 0){
                echo '<tr>';
            }
            $i++;
            echo '<td>' . $v->getLabelName() . ': <b>' . $v->labresult . '</b> ' . $v->unit . ' [' . $v->normal . ']' . '</td>';
            if ($i % 2 == 0) {
                echo '</tr>';
            }
        }
    }
    ?>
</table>