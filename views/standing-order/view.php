<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LStandingOrder */

$this->title = $model->standing_order_name;

\yii\web\YiiAsset::register($this);
?>
<div class="lstanding-order-view">

    <p>
        <?= Html::a('Update', ['update', 'standing_order_id' => $model->standing_order_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'standing_order_id' => $model->standing_order_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'standing_order_id',
            'standing_order_name',
            'user_id',
            'standing_order_type',
            'standing_order_map_code',
            'medusage',
            'qty',
            'is_active',
        ],
    ]) ?>

</div>
