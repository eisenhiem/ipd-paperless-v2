<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "consult_rep".
 *
 * @property int $rep_id
 * @property int $consult_id
 * @property int $order_id
 * @property int $user_id
 * @property string $rep_detail
 * @property string $rep_datetime
 * @property string|null $d_update
 */
class ConsultRep extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consult_rep';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['consult_id', 'order_id', 'user_id', 'rep_detail', 'rep_datetime'], 'required'],
            [['consult_id', 'order_id', 'user_id'], 'integer'],
            [['rep_detail'], 'string'],
            [['rep_datetime', 'd_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'rep_id' => 'Rep ID',
            'consult_id' => 'Consult ID',
            'order_id' => 'Order ID',
            'user_id' => 'ผู้บันทึก',
            'rep_detail' => 'รายละเอียด',
            'rep_datetime' => 'วันเวลา',
            'd_update' => 'D Update',
        ];
    }

    public function getProfile(){
        return $this->hasOne(Profile::className(),['user_id' => 'user_id']);
    }
    
}
