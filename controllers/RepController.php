<?php

namespace app\controllers;

use app\models\ConsultRep;
use app\models\ConsultRepSearch;
use app\models\DoctorOrders;
use app\models\Ipt;
use app\models\IptConsult;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RepController implements the CRUD actions for ConsultRep model.
 */
class RepController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all ConsultRep models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ConsultRepSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ConsultRep model.
     * @param int $rep_id Rep ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($rep_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($rep_id),
        ]);
    }

    /**
     * Creates a new ConsultRep model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($id)
    {
        $model = new ConsultRep();
        $con = IptConsult::find()->where(['consult_id' => $id])->one();
        $order = DoctorOrders::find()->where(['order_id' => $con->order_id])->one();
        $model->consult_id = $id;
        $model->order_id = $con->order_id;
        $model->user_id = Yii::$app->user->identity->id;
        $model->rep_datetime = date('Y-m-d H:i:s');
        if(Yii::$app->user->identity->role == 2) {
            $consult = 'เวชปฏิบัติฯ';
        }
        if(Yii::$app->user->identity->role == 4) {
            $consult = 'เภสัช';
        }
        if(Yii::$app->user->identity->role == 6) {
            $consult = 'กายภาพ';
        }
        if(Yii::$app->user->identity->role == 7) {
            $consult = 'ทันตกรรม';
        }
        if(Yii::$app->user->identity->role == 8) {
            $consult = 'แพทย์เฉพาะทาง';
        }

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                $order->progress_note = $order->progress_note . PHP_EOL . '['.$model->profile->fullname .':'.Ipt::getThaiDateTime($model->rep_datetime).PHP_EOL .$model->rep_detail.']';
                $order->save();                
                $con->consult_status = '1';
                $con->save();
                return $this->redirect(['consult/index', 'type' => $consult]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ConsultRep model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $rep_id Rep ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($rep_id)
    {
        $model = $this->findModel($rep_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'rep_id' => $model->rep_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ConsultRep model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $rep_id Rep ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($rep_id)
    {
        $this->findModel($rep_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ConsultRep model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $rep_id Rep ID
     * @return ConsultRep the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($rep_id)
    {
        if (($model = ConsultRep::findOne(['rep_id' => $rep_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
