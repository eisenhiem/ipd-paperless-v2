<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tpr".
 *
 * @property int $an
 * @property int $timetest
 * @property string $dttm
 * @property float $tt
 * @property int $pr
 * @property int $rr
 * @property int $sbp
 * @property int $dbp
 * @property int $intake
 * @property int $output
 * @property int $painscore
 * @property int $s
 * @property int $u
 * @property string $user
 * @property int $user_id
 */
class Tpr extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tpr';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'dttm', 'user_id'], 'required'],
            [['an', 'timetest', 'pr', 'rr', 'sbp', 'dbp', 'intake', 'output', 'painscore', 's', 'u','oral_fluid','parenteral','urine_fluid','emesis','drainage','aspiration'], 'integer'],
            [['dttm','d_update'], 'safe'],
            [['tt'], 'number'],
            [['record_time'], 'string', 'max' => 5],
            [['user'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'An',
            'timetest' => 'Timetest',
            'dttm' => 'Dttm',
            'tt' => 'Tt',
            'pr' => 'Pr',
            'rr' => 'Rr',
            'sbp' => 'Sbp',
            'dbp' => 'Dbp',
            'intake' => 'Intake',
            'output' => 'Output',
            'painscore' => 'Painscore',
            's' => 'S',
            'u' => 'U',
            'user' => 'User',
        ];
    }
}
