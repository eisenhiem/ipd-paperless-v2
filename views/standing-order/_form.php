<?php

use app\models\Profile;
use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$dr = ArrayHelper::map(User::find()->where(['>', 'role', 4])->joinWith('profile')->all(), 'id', 'profile.fullname');
/* @var $this yii\web\View */
/* @var $model app\models\LStandingOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lstanding-order-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-8 offset-2 col-sm-10 offset-1">
            <div class="card border-primary">
                <div class="card-header" style="background-color:cornflowerblue;color:white">
                    <?= $form->field($model, 'user_id')->dropDownList($dr,['prompt' => 'เลือกแพทย์']) ?>
                </div>
                <div class="card-body">

                    <?= $form->field($model, 'standing_order_name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'standing_order_type')->radioList(['PRC' => 'PRC', 'XRY' => 'XRY', 'LAB' => 'LAB', 'MED' => 'MED',], ['prompt' => '']) ?>

                    <?= $form->field($model, 'standing_order_map_code')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'medusage')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'qty')->textInput() ?>

                    <?= $form->field($model, 'is_active')->radioList([1 => 'Active', 0 => 'Disactive',]) ?>

                </div>
                <div class="card-footer">
                    <div class="form-group">
                        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>