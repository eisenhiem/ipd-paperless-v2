<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "l_standing_order".
 *
 * @property int $standing_order_id
 * @property string $standing_order_name
 * @property int|null $user_id
 * @property string|null $standing_order_type
 * @property string|null $standing_order_map_code
 * @property string|null $medusage
 * @property int|null $qty
 * @property string|null $is_active
 */
class LStandingOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'l_standing_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['standing_order_name'], 'required'],
            [['user_id', 'qty'], 'integer'],
            [['standing_order_type', 'is_active'], 'string'],
            [['standing_order_name'], 'string', 'max' => 255],
            [['standing_order_map_code', 'medusage'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'standing_order_id' => 'Standing Order ID',
            'standing_order_name' => 'Standing Order',
            'user_id' => 'แพทย์',
            'standing_order_type' => 'ประเภท',
            'standing_order_map_code' => 'Map Code',
            'medusage' => 'วิธีใช้',
            'qty' => 'จำนวน',
            'is_active' => 'Is Active',
        ];
    }
}
