<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "iprsc".
 *
 * @property string $namedrug
 * @property int $an
 * @property int $prscno
 * @property int $hn
 * @property string $prscdate
 * @property int $prsctime
 * @property string $meditem
 * @property string $medusage
 * @property int $qty
 * @property int $xdoseno
 * @property int $lcontinue
 * @property string $offdate
 * @property string $offtime
 * @property string $admtime
 * @property int $id
 */
class Iprsc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'iprsc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['namedrug', 'an', 'prscno', 'hn', 'prsctime', 'meditem', 'medusage', 'qty', 'xdoseno', 'offtime', 'admtime'], 'required'],
            [['an', 'prscno', 'hn', 'prsctime', 'qty', 'xdoseno', 'lcontinue'], 'integer'],
            [['prscdate', 'offdate'], 'safe'],
            [['namedrug'], 'string', 'max' => 50],
            [['meditem'], 'string', 'max' => 7],
            [['medusage'], 'string', 'max' => 5],
            [['offtime'], 'string', 'max' => 60],
            [['admtime'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'namedrug' => 'Namedrug',
            'an' => 'An',
            'prscno' => 'Prscno',
            'hn' => 'Hn',
            'prscdate' => 'Prscdate',
            'prsctime' => 'Prsctime',
            'meditem' => 'Meditem',
            'medusage' => 'Medusage',
            'qty' => 'Qty',
            'xdoseno' => 'Xdoseno',
            'lcontinue' => 'Lcontinue',
            'offdate' => 'Offdate',
            'offtime' => 'Offtime',
            'admtime' => 'Admtime',
            'id' => 'ID',
        ];
    }
}
