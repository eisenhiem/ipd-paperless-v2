<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;

$this->title = 'AN:' . $an;
\yii\web\YiiAsset::register($this);

?>
<p>
    &emsp; <?= Html::a(Icon::show('chevron-left') . ' กลับ', ['note/list', 'id' => $an], ['class' => 'btn btn-danger', 'style' => 'width:200px']) ?>
    &emsp; <?php //echo Html::a(Icon::show('print') . ' พิมพ์', ['print-vitalsign', 'id' => $an], ['class' => 'btn btn-info', 'style' => 'width:200px']) ?>
    <br>
</p>
<?php

echo Highcharts::widget([
    'options' => [
        'chart' => [
            'type' => 'line',
        ],
        'title' => ['text' => 'V/S'],
        'xAxis' => [
            'title' => 'วันเวลา',
            'categories' => $date
        ],

        'yAxis' => [
            [ // primary axis
                'title' => ['text' => 'temp  ( ํc)'],
                'min' => 35,
                'max' => 42,
            ],
            [ // secondary axis
                'gridLineWidth' => 0,
                'title' => ['text' => 'Pluse Rate /min'],
                'max' => 160,
                'min' => 40,
                'opposite' => true,
            ]
        ],
        'series' => [
            [
                'yAxis' => 0,
                'name' => 'BT',
                'data' => $bt,
                'color' => '#5070D0',
            ],
            [
                'yAxis' => 1,
                'name' => 'pluse rate',
                'data' => $pr,
                'color' => '#DF5080',
            ],
        ]
    ]
]);
?>

<table width="100%" border=1>
    <tr>
        <th>Date</th>
        <?php
        foreach ($date as $d) {
            echo '<th>' . $d . '</th>';
        }
        ?>
    </tr>
    <tr>
        <td>RR</td>
        <?php
        foreach ($rr as $d) {
            if ($d == 0) {
                $d = 'N/A';
            }
            echo '<td>' . $d . '</td>';
        }
        ?>
    </tr>
    <tr>
        <td>SBP</td>
        <?php
        foreach ($sbp as $d) {
            if ($d == 0) {
                $d = 'N/A';
            }
            echo '<td>' . $d . '</td>';
        }
        ?>
    </tr>
    <tr>
        <td>DBP</td>
        <?php
        foreach ($dbp as $d) {
            if ($d == 0) {
                $d = 'N/A';
            }
            echo '<td>' . $d . '</td>';
        }
        ?>
    </tr>
    <tr>
        <td>In Take</td>
        <?php
        foreach ($intake as $d) {
            if ($d == 0) {
                $d = 'N/A';
            }
            echo '<td>' . $d . '</td>';
        }
        ?>
    </tr>
    <tr>
        <td>Out Take</td>
        <?php
        foreach ($outtake as $d) {
            if ($d == 0) {
                $d = 'N/A';
            }
            echo '<td>' . $d . '</td>';
        }
        ?>
    </tr>
    <tr>
        <td>Stools</td>
        <?php
        foreach ($stool as $d) {
            if ($d == 0) {
                $d = 'N/A';
            }
            echo '<td>' . $d . '</td>';
        }
        ?>
    </tr>
    <tr>
        <td>Urines</td>
        <?php
        foreach ($urine as $d) {
            if ($d == 0) {
                $d = 'N/A';
            }
            echo '<td>' . $d . '</td>';
        }
        ?>
    </tr>

</table>