<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "allergy".
 *
 * @property string $namedrug
 * @property int $hn
 * @property string $meditem
 * @property string $allgtype
 * @property string $detail
 * @property string $entrydate
 * @property string $alevel
 * @property string $informant
 * @property string $informhosp
 * @property string $pharmacist
 * @property int $id
 */
class Allergy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'allergy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['namedrug', 'hn', 'meditem', 'allgtype', 'detail', 'alevel', 'informant', 'informhosp', 'pharmacist'], 'required'],
            [['hn'], 'integer'],
            [['detail'], 'string'],
            [['entrydate'], 'safe'],
            [['namedrug'], 'string', 'max' => 50],
            [['meditem'], 'string', 'max' => 7],
            [['allgtype'], 'string', 'max' => 1],
            [['alevel', 'informant'], 'string', 'max' => 2],
            [['informhosp'], 'string', 'max' => 5],
            [['pharmacist'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'namedrug' => 'Namedrug',
            'hn' => 'Hn',
            'meditem' => 'Meditem',
            'allgtype' => 'Allgtype',
            'detail' => 'Detail',
            'entrydate' => 'Entrydate',
            'alevel' => 'Alevel',
            'informant' => 'Informant',
            'informhosp' => 'Informhosp',
            'pharmacist' => 'Pharmacist',
            'id' => 'ID',
        ];
    }
}
