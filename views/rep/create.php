<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ConsultRep */

$this->title = 'ตอบกลับ Consult';

?>
<div class="consult-rep-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
