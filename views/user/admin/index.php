<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use app\models\HiSetup;
use app\models\Profile;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use \dektrium\user\models\User;
use kartik\grid\GridView;

$user = User::find()->all();

/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \dektrium\user\models\UserSearch $searchModel
 */

$layout = HiSetup::find()->where(['common_code'=>'layout'])->one();

$this->title = Yii::t('user', 'จัดการผู้ใช้');
?>

<p>
    &emsp; <?= Html::a(Icon::show('user-plus').' Add User', ['create'], ['class' => 'btn btn-success']) ?> 
    &emsp; <?= Html::a('Switch Layout', ['/admission/layout'], ['class' => 'btn btn-primary']) ?>

</p>

<?php 
if ($layout->value2 == 'grid'){
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'layout'       => "{items}\n{pager}",
    'columns' => [
        [
            'attribute' => 'id',
            'headerOptions' => ['style' => 'width:90px;'], # 90px is sufficient for 5-digit user ids
        ],
        'username',
        [
            'header' => 'ชื่อ-สกุล',
            'value' => function($model){
                return $model->profile->fullname;
            }
        ],
        'email:email',
        [
            'header' => Yii::t('user', 'Confirmation'),
            'value' => function ($model) {
                if ($model->isConfirmed) {
                    return '<div class="text-center">
                                <span class="text-success">' . Yii::t('user', 'Confirmed') . '</span>
                            </div>';
                } else {
                    return Html::a(Yii::t('user', 'Confirm'), ['confirm', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-success btn-block',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to confirm this user?'),
                    ]);
                }
            },
            'format' => 'raw',
            'visible' => Yii::$app->getModule('user')->enableConfirmation,
        ],
        [
            'header' => Yii::t('user', 'Block status'),
            'value' => function ($model) {
                if ($model->isBlocked) {
                    return Html::a(Yii::t('user', 'Unblock'), ['block', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-success btn-block',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                    ]);
                } else {
                    return Html::a(Yii::t('user', 'Block'), ['block', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-danger btn-block',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                    ]);
                }
            },
            'format' => 'raw',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {profile} {role} ',
            'buttons' => [
                'profile' => function($url,$model,$key){
                    return Html::a(Icon::show('id-card'), ['/profile/update','user_id'=>$model->id], ['class' => 'btn btn-info','style' =>['width'=>'100px']]) ;
                  },
                  'update' => function($url,$model,$key){
                    return Html::a(Icon::show('key'), ['update','id'=>$model->id], ['class' => 'btn btn-info','style' =>['width'=>'100px']]) ;
                  },
                  'role' => function($url,$model,$key){
                    return Html::a(Icon::show('key'), ['/profile/change','user_id'=>$model->id], ['class' => 'btn btn-info','style' =>['width'=>'100px']]) ;
                  }
        
                ]
        ],
    ],
]);

} else {
?>
<div class="row">
    <?php
    foreach ($user as $u) { ?>

        <div class="col-md-4 col-sm-6 col-12">
            <div class="card text-right">
                <div class="card-body">
                    <div class="card-title" style="font-size: 24pt;color:#1572A1;"><?= $u->username ?></div><br>
                    <div class="card-text"><?= $u->profile->fullname ?><br><?= $u->profile->position ?><br></div>
                </div> 
                <div class="card-footer">
                <?= Html::a(Icon::show('id-card') . ' Profile', ['/profile/update','user_id'=>$u->id], ['class' => 'btn btn-info btn-block']) ?>
                <?= Html::a(Icon::show('key').' User/Pass', ['update','id'=>$u->id], ['class' => 'btn btn-warning btn-block']) ?>
                <?= Html::a(Icon::show('key').' Change Role', ['/profile/change','user_id'=>$u->id], ['class' => 'btn btn-primary btn-block']) ?>
                </div>
            </div>

            <!-- END your <body> content of the Card above this line, right before "Card::end()"  -->
        </div>

    <?php }
    ?>
</div>
<?php 
}
?>
