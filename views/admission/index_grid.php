<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RoundsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูลผู้ป่วย '.$ward->nameidpm;
?>
<div class="admission-index">

<div style="text-align:right">
    <?= Html::a(Icon::show('id-card'). 'บัตร', ['switchlayout','id' => $ward->idpm], ['class' => 'btn btn-primary']) ?>
    <?php echo $this->render('_search', ['model' => $searchModel,'id'=>$ward->idpm]); ?>
</div>
<br>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' =>[
          'before'=>' '
        ],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
              'header' => 'เตียง',
              'value' => function ($model) {
                return $model->iptadm->getBedName();
              },
            ],
            'an',
          'hn',
          [
            'header' => 'ชื่อ-สกุล',
            'value' => function ($model) {
              return $model->pt->getFullName();
            },
          ],
          [
            'attribute' => 'gender',
            'value' => function ($model) {
              return $model->pt->getGender();
            },
          ],
          [
            'header' => 'อายุ',
            'value' => function ($model) {
              return $model->pt->getAge();
            },
          ],
          [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'ดูข้อมูล',
            'options'=>['style'=>'width:60px;'],
            'template'=>'{view}',
            'buttons'=>[
              'view' => function($url,$model,$key){
                return Html::a(Icon::show('eye'), ['view','an'=>$model->an], ['class' => 'btn btn-info','style' =>['width'=>'60px']]) ;
              }
            ]
          ],
          [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Order',
            'options'=>['style'=>'width:60px;'],
            'template'=>'{order}',
            'buttons'=>[
              'order' => function($url,$model,$key){
                return Html::a(Icon::show('notes-medical'), ['order/list','id'=>$model->an], ['class' => $model->getReceive(),'style' =>['width'=>'60px']]) ;
              }
            ]
          ],
          [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Note',
            'options'=>['style'=>'width:60px;'],
            'template'=>'{note}',
            'buttons'=>[
              'note' => function($url,$model,$key){
                return Html::a(Icon::show('clipboard'), ['note/list','id'=>$model->an], ['class' => 'btn btn-warning','style' =>['width'=>'60px']]) ;
              }
            ]
          ],
        ],
    ]); ?>


</div>
