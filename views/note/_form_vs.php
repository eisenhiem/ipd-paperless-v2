<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Rounds;
use kartik\date\DatePicker;
use kartik\icons\Icon;
use yii\widgets\MaskedInput;

$round = [2 => 'รอบ 2.00 น.', 6 => 'รอบ 6.00 น.', 10 => 'รอบ 10.00 น.', 14 => 'รอบ 14.00 น.', 18 => 'รอบ 18.00 น.', 22 => 'รอบ 22.00 น.'];

/* @var $this yii\web\View */
/* @var $model app\models\Notes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notes-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card bg-primary">
        <div class="card-header">
            <div class="row">
            <div class="col-3" style="text-align:right;">เวลาที่วัด: </div>
            <div class="col-3">
                    <?= $form->field($model,'record_time')->widget(
                        MaskedInput::className(),
                        [
                            'mask' => '99:99'
                        ])->label(false); 
                    ?>
                </div>
            </div>
        </div>
        <div class="card-body bg-light">
            <div class="card">
                <div class="card-header">Vital Sign</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-1" style="text-align:right;">BT:</div>
                        <div class="col-1">
                            <?= $form->field($model, 'tt')->textInput()->label(false) ?>
                        </div>
                        <div class="col-1" style="text-align:right;">PR:</div>
                        <div class="col-1">
                            <?= $form->field($model, 'pr')->textInput()->label(false) ?>
                        </div>
                        <div class="col-1" style="text-align:right;">RR:</div>
                        <div class="col-1">
                            <?= $form->field($model, 'rr')->textInput()->label(false) ?>
                        </div>
                        <div class="col-1" style="text-align:right;">BP:</div>
                        <div class="col-2">
                            <?= $form->field($model, 'sbp')->textInput()->label(false) ?>
                        </div>
                        <div class="col-1" style="text-align:right;">/</div>
                        <div class="col-2">
                            <?= $form->field($model, 'dbp')->textInput()->label(false) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">Intake (ml)</div>
                <div class="card-body">
                    <div class="row">
                    <div class="col-2" style="text-align:right;">Oral:</div>
                        <div class="col-4">
                            <?= $form->field($model, 'oral_fluid')->textInput()->label(false) ?>
                        </div>
                        <div class="col-2" style="text-align:right;">parenteral:</div>
                        <div class="col-4">
                            <?= $form->field($model, 'parenteral')->textInput()->label(false) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">Outtake (ml)</div>
                <div class="card-body">
                    <div class="row">
                    <div class="col-2" style="text-align:right;">Urine:</div>
                        <div class="col-4">
                            <?= $form->field($model, 'urine_fluid')->textInput()->label(false) ?>
                        </div>
                        <div class="col-2" style="text-align:right;">Emesis:</div>
                        <div class="col-4">
                            <?= $form->field($model, 'emesis')->textInput()->label(false) ?>
                        </div>
                        <div class="col-2" style="text-align:right;">Drainage:</div>
                        <div class="col-4">
                            <?= $form->field($model, 'drainage')->textInput()->label(false) ?>
                        </div>
                        <div class="col-2" style="text-align:right;">Aspiration:</div>
                        <div class="col-4">
                            <?= $form->field($model, 'aspiration')->textInput()->label(false) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">Stool/Urine (ครั้ง)</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-2" style="text-align:right;">Urine:</div>
                        <div class="col-4">
                            <?= $form->field($model, 'u')->textInput()->label(false) ?>
                        </div>
                        <div class="col-2" style="text-align:right;">Stool:</div>
                        <div class="col-4">
                            <?= $form->field($model, 's')->textInput()->label(false) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer bg-light">
            <div class="form-group" style="text-align: center;">
                <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success', 'style' => 'width:150px']) ?>

                <?= Html::a(Icon::show('chevron-left') . ' กลับไปหน้าหลัก', ['list', 'id' => $model->an], ['class' => 'btn btn-danger', 'style' => 'width:150px']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>