<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IptConsultSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ipt-consult-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'consult_id') ?>

    <?= $form->field($model, 'consult_type') ?>

    <?= $form->field($model, 'order_id') ?>

    <?= $form->field($model, 'consult_description') ?>

    <?= $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'consult_datetime') ?>

    <?php // echo $form->field($model, 'u_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
