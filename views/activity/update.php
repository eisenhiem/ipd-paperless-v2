<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LActivity */

$this->title = 'Update L Activity: ' . $model->activity_id;

?>
<div class="lactivity-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
