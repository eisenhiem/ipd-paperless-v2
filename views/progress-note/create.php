<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LProgressNote */

$this->title = 'เพิ่ม progress Note';

?>
<div class="lprogress-note-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
