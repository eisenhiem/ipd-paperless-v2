<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IptOwnerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ipt-owner-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'owner_id') ?>

    <?= $form->field($model, 'an') ?>

    <?= $form->field($model, 'owner_type') ?>

    <?= $form->field($model, 'owner_user_id') ?>

    <?= $form->field($model, 'owner_status') ?>

    <?php // echo $form->field($model, 'd_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
