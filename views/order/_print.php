<?php

use yii\helpers\Html;
use app\models\Admission;
use app\models\Ipt;

function thaimonth($m)
{
  switch ($m) {
    case '01':
      $thainame = 'มกราคม';
      break;
    case '02':
      $thainame = 'กุมภาพันธ์';
      break;
    case '03':
      $thainame = 'มีนาคม';
      break;
    case '04':
      $thainame = 'เมษายน';
      break;
    case '05':
      $thainame = 'พฤษภาคม';
      break;
    case '06':
      $thainame = 'มิถุนายน';
      break;
    case '07':
      $thainame = 'กรกฎาคม';
      break;
    case '08':
      $thainame = 'สิงหาคม';
      break;
    case '09':
      $thainame = 'กันยายน';
      break;
    case '10':
      $thainame = 'ตุลาคม';
      break;
    case '11':
      $thainame = 'พฤศจิกายน';
      break;
    case '12':
      $thainame = 'ธันวาคม';
      break;
  }
  return $thainame;
}
?>
<?php
$page = 1;
$row = 0;
foreach ($model as $order) {
  if ($page <> 1 && $row % 2 == 1 && $row > 2 || $row == 1) {
    echo '<pagebreak>';
    $page++;
  }
  $row++;
  // echo '- '.$page.' - '.$row;
?>
  <?php
  if ($page == 1 || $row % 2 == 0) {
  ?>
    <h3 align="center"><?= $office->value2 ?><br><?= $office->value3 ?> <?= $office->values4 ?>
      <br>Doctor Orders Sheet & Progress Note
    </h3>
  <?php
  }
  ?>
  <table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
    <tr>
      <?php
      if ($page == 1 || $row % 2 == 0) {
      ?>
        <td width="35%"><b>&emsp; ชื่อ-สกุล </b><br>&emsp; &emsp; &emsp;<?= $pt->pt->getFullName() ?></td>
        <td width="25%"><b>&emsp;Ward </b> <?= $order->getWardname() ?><br>&emsp; <b>Bed</b> <?= $order->bed_no ?></td>
        <td width="20%">&emsp; <b>อายุ</b> <?= $pt->pt->getAge() ?><br>&emsp; <b>เพศ </b> <?= $pt->pt->getGender() ?></td>
        <td width="20%">&emsp; <b>HN:</b> <?= $pt->hn ?><br>&emsp; <b>AN:</b> <?= $pt->an ?></td>
      <?php
      } else {
        echo '<td colspan="4"><b>&emsp;Ward </b>' . $order->getWardname() . ' &emsp; <b>Bed</b> ' . $order->bed_no . '</td>';
      }
      ?>
    </tr>
  </table>
  <table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
    <tr>
      <th width="25%">Progress Note</th>
      <th width="25%">Order One Day</th>
      <th width="35%">Order Continue</th>
      <th width="15%">หมายเหตุ</th>
    </tr>
    <tr>
      <?php if ($row == 1) {
        echo '<td height=720 valign=top style="padding-left:5px;padding-right:5px;">';
      } else {
        echo '<td height=350 valign=top style="padding-left:5px;padding-right:5px;">';
      }
      ?>
      <?= Ipt::getThaiDateTime($order->order_datetime) ?><br>
      <?= $order->specialist_progress_note ? $order->getProgName() . '<br>' : '' ?>
      <?= nl2br($order->progress_note) ?><br>
      <b>Order By :</b><br>
      <?= $order->profile->fullname ?><br>
      <?= $order->profile->position ?><br>
      <?= $order->profile->license_no ?><br>
      <?= $order->receive_id ? '<b>รับ Order : </b>'.$order->receive->fullname.'<br>'.$order->receive->position:'' ?>

      <td valign=top style="padding-left:5px;padding-right:5px;">
        <?= $order->labs ? '<b><u>Lab</u> ::</b><br>' . $order->getLabsName() . '<br>' : '' ?>
        <?= $order->xrays ? '<b><u>X-Ray</u> ::</b><br>' . $order->getXraysName() . '<br>' : '' ?>
        <?= nl2br($order->order_oneday) ?>
      <td valign=top style="padding-left:5px;padding-right:5px;">
        <?= $order->food ? $order->getFoodName() : '' ?>
        <?= $order->extra_food ? ' ระบุ ' . $order->extra_food . '<br>' : '<br>' ?>
        <?= $order->record_io ? '&emsp; [ / ] Record I/O <br>' : '' ?>
        <?= $order->record_vitalsign ? '&emsp; [ / ] Record V/S <br>' : '' ?>
        <?= $order->order_continue ? nl2br($order->order_continue) : '' ?>
      </td>
      <td style="padding-left:5px;padding-right:5px;vertical-align:top">
        <?= $order->order_comment ?>
        <br>
        <?= $order->receive_id ? '<b>รับ Order By:</b>' : '' ?>
        <?= $order->receive->fullname ?><br>
        <?= $order->receive->position ?><br>
      </td>
    </tr>
  </table>
<?php

} ?>