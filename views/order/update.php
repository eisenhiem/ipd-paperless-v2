<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DoctorOrders */

$this->title = 'Update Doctor Orders: ' . $model->order_id;
?>
<div class="doctor-orders-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
