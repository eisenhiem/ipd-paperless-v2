<?php

use yii\helpers\Html;
use app\models\VsDaily;
use app\models\Admission;
use app\models\Ipt;

function thaimonth($m)
{
  switch ($m) {
    case '01':
      $thainame = 'มกราคม';
      break;
    case '02':
      $thainame = 'กุมภาพันธ์';
      break;
    case '03':
      $thainame = 'มีนาคม';
      break;
    case '04':
      $thainame = 'เมษายน';
      break;
    case '05':
      $thainame = 'พฤษภาคม';
      break;
    case '06':
      $thainame = 'มิถุนายน';
      break;
    case '07':
      $thainame = 'กรกฎาคม';
      break;
    case '08':
      $thainame = 'สิงหาคม';
      break;
    case '09':
      $thainame = 'กันยายน';
      break;
    case '10':
      $thainame = 'ตุลาคม';
      break;
    case '11':
      $thainame = 'พฤศจิกายน';
      break;
    case '12':
      $thainame = 'ธันวาคม';
      break;
  }
  return $thainame;
}
?>
<?php
$i = 1;
foreach ($model as $note) {
  if ($i <> 1) {
    echo '<pagebreak>';
  }
  $i++;
?>
  <h3 align="center"><?= $office->value2 ?><br><?= $office->value3 ?> <?= $office->values4 ?>
    <br>บันทึกทางการพยาบาล
  </h3>
  <table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
    <tr>
      <td width="35%"><b>&emsp; ชื่อ-สกุล </b><br>&emsp; &emsp; &emsp;<?= $pt->pt->getFullName() ?></td>
      <td width="35%">&emsp; <b>อายุ</b> <?= $pt->pt->getAge() ?> ปี<br>&emsp; <b>เพศ </b> <?= $pt->pt->getGender() ?></td>
      <td width="30%">&emsp; <b>HN:</b> <?= $pt->hn ?><br>&emsp; <b>AN:</b> <?= $pt->an ?></td>
    </tr>
  </table>
  <table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
    <tr>
      <th width="20%">Date/Time</th>
      <th width="20%">Problem</th>
      <th width="25%">Activity</th>
      <th width="35%">Evaluate</th>
    </tr>
    <tr>
      <td height=720 valign=top align="center">
        <?= Ipt::getThaiDate($note->note_date) . '<br>' . $note->note_time . ' น.' ?>
        <br>
      </td>
      <td valign=top style="padding:5px;">
        <?= $note->activity_select ? $note->getActName() : '' ?>
        <?= nl2br($note->activity) ?>
      </td>
      <td style="vertical-align:top;padding:5px;">
        <?= $note->evaluate_select ? $note->getEvaname() : '' ?>
        <?= nl2br($note->activity) ?>
      </td>
      <td valign=top style="padding:5px;">
        &emsp;<?= nl2br($note->evaluate) ?><br>
        <br><b>ผู้ประเมิน</b><br><br>
        <?= $note->profile->fullname ?><br>
        <?= $note->profile->position ?><br>

      </td>
    </tr>
  </table>
<?php
}
?>