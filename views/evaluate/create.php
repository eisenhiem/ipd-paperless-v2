<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LEvaluate */

$this->title = 'Create L Evaluate';
?>
<div class="levaluate-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
