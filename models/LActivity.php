<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "l_activity".
 *
 * @property string $activity_id
 * @property string|null $activity_name
 * @property string|null $is_active
 */
class LActivity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'l_activity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activity_id'], 'required'],
            [['is_active'], 'string'],
            [['activity_id'], 'string', 'max' => 2],
            [['activity_name'], 'string', 'max' => 255],
            [['activity_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'activity_id' => 'Activity ID',
            'activity_name' => 'Activity Name',
            'is_active' => 'Is Active',
        ];
    }
}
