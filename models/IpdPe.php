<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ipd_pe".
 *
 * @property int $an
 * @property int $problem
 */
class IpdPe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ipd_pe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'problem'], 'required'],
            [['an', 'problem'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'An',
            'problem' => 'Problem',
        ];
    }
}
