<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IptOwner */

$this->title = 'เพิ่มแพทย์';
?>
<div class="ipt-owner-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
