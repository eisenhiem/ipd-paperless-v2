/*
Navicat MySQL Data Transfer

Source Server         : hi_server
Source Server Version : 50568
Source Host           : 192.168.111.5:3306
Source Database       : hi

Target Server Type    : MYSQL
Target Server Version : 50568
File Encoding         : 65001

Date: 2022-06-23 14:46:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `doctor_orders`
-- ----------------------------
DROP TABLE IF EXISTS `doctor_orders`;
CREATE TABLE `doctor_orders` (
`order_id`  int(11) NOT NULL AUTO_INCREMENT ,
`an`  int(11) NOT NULL ,
`ward`  varchar(2) CHARACTER SET tis620 COLLATE tis620_thai_ci NULL DEFAULT NULL ,
`bed_no`  varchar(10) CHARACTER SET tis620 COLLATE tis620_thai_ci NULL DEFAULT NULL ,
`order_datetime`  datetime NOT NULL ,
`progress_note`  text CHARACTER SET tis620 COLLATE tis620_thai_ci NULL ,
`order_oneday`  text CHARACTER SET tis620 COLLATE tis620_thai_ci NULL ,
`order_continue`  text CHARACTER SET tis620 COLLATE tis620_thai_ci NULL ,
`order_comment`  text CHARACTER SET tis620 COLLATE tis620_thai_ci NULL ,
`user_id`  int(11) NOT NULL ,
`d_update`  timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
`food`  varchar(2) CHARACTER SET tis620 COLLATE tis620_thai_ci NULL DEFAULT NULL ,
`record_io`  varchar(1) CHARACTER SET tis620 COLLATE tis620_thai_ci NULL DEFAULT NULL ,
`record_vitalsign`  varchar(1) CHARACTER SET tis620 COLLATE tis620_thai_ci NULL DEFAULT NULL ,
PRIMARY KEY (`order_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=tis620 COLLATE=tis620_thai_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of doctor_orders
-- ----------------------------

-- ----------------------------
-- Table structure for `l_activity`
-- ----------------------------
DROP TABLE IF EXISTS `l_activity`;
CREATE TABLE `l_activity` (
`activity_id`  varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`activity_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`is_active`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' ,
PRIMARY KEY (`activity_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of l_activity
-- ----------------------------
BEGIN;
INSERT INTO `l_activity` VALUES ('01', 'แนะนำการปฏิบัติตัวเมื่ออยู่รพ./HI/CI', '1'), ('02', 'อธิบายแนวทางการรักษาและการปฏิบัติตัวเพื่อให้ผู้ป่วยทราบเบื้องต้น', '1'), ('03', 'วัด V/S ,O2sat ', '1'), ('04', 'แนะนำอาคาร สถานที่ เวลาการรับประทานอาหาร', '1'), ('05', 'แนะนำการสังเกตอาการผิดปกติที่ต้องแจ้งเจ้าหน้าที่ เช่น ไข้ ไอ หอบ หายใจไม่อิ่ม', '1'), ('06', 'ซักถามอาการผู้ป่วย', '1'), ('07', 'ประเมินสภาพร่างกายทั่วไปก่อนจำหน่าย', '1'), ('08', 'ให้คำแนะนำการปฏิบัติตัวหลังจากจำหน่ายคนไข้กลับบ้าน', '1'), ('09', 'ประเมิน 2 Q = negative', '1'), ('10', 'จัดชุด Gift set พร้อมแนะนำการรับประทานยา การบริหารร่างกาย', '1'), ('11', 'ส่งตรวจ lab, CXR', '1'), ('12', 'แจ้งผล Lab และ ผล X-rays ให้ผู้ป่วยทราบ', '1'), ('14', 'เปิดโอกาสให้ซักถามข้อสงสัย', '1'), ('15', 'จำหน่ายออกจากการดูแล Home ward', '1'), ('16', 'แจ้งผู้ป่วยเตรียมตัวเข้า Admit ที่ รพ. เหล่าเสือโก้ก', '1'), ('17', 'ดูแลให้ยาFavipiravir ตามแผนการรักษา', '1'), ('18', 'ดูแลต่อเนื่องDay11-14', '1'), ('20', 'แนะนำการปฏิบัติตัว day 15 ถึง 28', '1');
COMMIT;

-- ----------------------------
-- Table structure for `l_evaluate`
-- ----------------------------
DROP TABLE IF EXISTS `l_evaluate`;
CREATE TABLE `l_evaluate` (
`evaluate_id`  varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`evaluate_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`is_active`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' ,
PRIMARY KEY (`evaluate_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of l_evaluate
-- ----------------------------
BEGIN;
INSERT INTO `l_evaluate` VALUES ('01', 'ผู้ป่วยรู้สึกตัวดี', '1'), ('02', 'ผู้ป่วยเข้าใจ ปฏิบัติตัวได้ถูกต้อง', '1'), ('03', 'รับประทานยาได้ ไม่มีอาการแพ้หรือสงสัยแพ้ยา', '1'), ('04', 'รับประทานยาFavipiravir ได้ ไม่มีคลื่นไส้อาเจียน ไม่มีถ่ายเหลว', '1'), ('05', 'ไอ', '1'), ('06', 'มีไข้', '1'), ('07', 'หอบ', '1'), ('08', 'เหนื่อยเพลีย', '1'), ('09', 'นอนไม่หลับ', '1'), ('10', 'ทานอาหารได้ปกติ', '1'), ('11', 'นอนหลับพักผ่อนได้ปกติ', '1');
COMMIT;

-- ----------------------------
-- Table structure for `nurse_notes`
-- ----------------------------
DROP TABLE IF EXISTS `nurse_notes`;
CREATE TABLE `nurse_notes` (
`note_id`  int(11) NOT NULL AUTO_INCREMENT ,
`an`  int(11) NOT NULL ,
`note_date`  date NOT NULL ,
`note_time`  varchar(5) CHARACTER SET tis620 COLLATE tis620_thai_ci NULL DEFAULT NULL ,
`activity`  text CHARACTER SET tis620 COLLATE tis620_thai_ci NOT NULL ,
`evaluate`  text CHARACTER SET tis620 COLLATE tis620_thai_ci NULL ,
`user_id`  int(11) NOT NULL ,
`activity_select`  text CHARACTER SET tis620 COLLATE tis620_thai_ci NULL ,
`evaluate_select`  text CHARACTER SET tis620 COLLATE tis620_thai_ci NULL ,
`d_update`  timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
PRIMARY KEY (`note_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=tis620 COLLATE=tis620_thai_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of nurse_notes
-- ----------------------------

-- ----------------------------
-- Table structure for `profile`
-- ----------------------------
DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
`user_id`  int(11) NOT NULL ,
`department_id`  int(11) NULL DEFAULT NULL ,
`sub_department_id`  int(11) NULL DEFAULT NULL ,
`fullname`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`cid`  varchar(13) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`position`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`position_level`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`license_no`  varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`user_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of profile
-- ----------------------------
BEGIN;
INSERT INTO `profile` VALUES ('1', '11', '37', 'นายจักรพงษ์ วงศ์กมลาไสย', '', 'นักวิชาการคอมพิวเตอร์ปฏิบัติการ', '', null), ('2', null, null, null, null, null, null, null), ('3', null, null, null, null, null, null, null), ('4', null, null, null, null, null, null, null), ('5', null, null, null, null, null, null, null), ('6', null, null, null, null, null, null, null), ('7', null, null, null, null, null, null, null);
COMMIT;

-- ----------------------------
-- Table structure for `token`
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
`user_id`  int(11) NOT NULL ,
`code`  varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`created_at`  int(11) NOT NULL ,
`type`  smallint(6) NOT NULL ,
FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
UNIQUE INDEX `token_unique` (`user_id`, `code`, `type`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci

;

-- ----------------------------
-- Records of token
-- ----------------------------
BEGIN;
INSERT INTO `token` VALUES ('1', 'ycfHswUqdZuUh748i0C0Dstqt_KfctWC', '1519701696', '0'), ('3', 'zqy9uOtwbMrJenW6GelM_9imjmy1nkLK', '1641871211', '0');
COMMIT;

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`username`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`email`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`password_hash`  varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`auth_key`  varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`confirmed_at`  int(11) NULL DEFAULT NULL ,
`unconfirmed_email`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`blocked_at`  int(11) NULL DEFAULT NULL ,
`registration_ip`  varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`created_at`  int(11) NOT NULL ,
`updated_at`  int(11) NOT NULL ,
`last_login_at`  int(11) NOT NULL DEFAULT 0 ,
`flags`  int(11) NOT NULL DEFAULT 0 ,
`role`  int(1) UNSIGNED NOT NULL DEFAULT 0 ,
PRIMARY KEY (`id`),
UNIQUE INDEX `user_unique_email` (`email`) USING BTREE ,
UNIQUE INDEX `user_unique_username` (`username`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci
AUTO_INCREMENT=8

;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('1', 'admin', 'jwongkamalasai@gmail.com', '$2y$12$/5wZnVSltPCI8XSqVjpU2.L1IZGV.zwbTUaHdA9zIm7u0OK9N8YX2', 'psQonEtwnqCrhKDcYghEKw_QaVj0FO9G', null, null, null, '192.168.111.3', '1531186376', '1531186376', '1655886004', '0', '9'), ('2', '3350400168059', 'suraparat@gmail.com', '$2y$12$BgnELiNpMsgdGC6O.yBi9.FaKKssmlMT6kp1ryaqpGLH49Lm86O7i', 'GrAYeSY0MRgDbEJKVRfFlQzs9V_b5pW7', '1641373019', null, null, '192.168.111.76', '1641373019', '1641373019', '1646637616', '0', '0'), ('3', 'gift', 'giftrx17@gmail.com', '$2y$12$a4tD/eBeFQTkSw95.r3HRupKqa/cqWR4xL9r5Dw5M1xbJ1G6F1iZi', 'l8_I_yfYIzITwxeDBBAtTZTnUssDRzkl', null, null, null, '192.168.111.183', '1641871211', '1641871211', '1641871286', '0', '0'), ('4', 'yam', 'kanniyammy@gmail.com', '$2y$12$cvODlPrHAeWOPQ/MHCT69OckMZgf.b0.L9nfh6zGX8bcMJvFH7Fg6', 'KhlUJoA7_zHNP1-yVdpvB1HOtupUE-c_', '1642411080', null, null, '192.168.111.114', '1642411080', '1642411080', '1655868684', '0', '0'), ('5', 'jantima', 'nij_ms@hotmail.com', '$2y$12$RErEfx.uASnyZajXJxZCsuj61p05ezSw1ZuVh5ZdtzhGEdbWSpZqy', '2uQNDPPJnkMoZP7a5whokOdGsONkD_ix', '1643188955', null, null, '192.168.111.72', '1643188955', '1651544977', '1655105290', '0', '0'), ('6', 'pongsakon', 'pongsakon.p36@gmail.com', '$2y$12$9Dy6vutYYwMLo.Yqz0Z.weVqK67/PzdMq9Z2azRWvU4WjrnSW.u0u', 'bMWfHluQPRmumthZHt6_GL0eg_5Txjor', '1651544875', null, null, '192.168.111.191', '1651544875', '1651544875', '1652243818', '0', '0'), ('7', 'sai', 'xxxx@xxx.com', '$2y$12$nOm3nIyR93hVm1CzFaOLfODhSDjrCN0s4.7io1ffkZJfrCT9PKayq', '8XhBg7qcgqHdPrWgrU5I_CeyUfbN1hGA', '1653879476', null, null, '192.168.111.7', '1653879476', '1653879476', '1655967878', '0', '0');
COMMIT;

-- ----------------------------
-- View structure for `ipd_pe`
-- ----------------------------
DROP VIEW IF EXISTS `ipd_pe`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `ipd_pe` AS select `i`.`an` AS `an`,group_concat(`x`.`sign` order by `x`.`id` ASC separator '<br />') AS `problem` from (`ipt` `i` join `sign` `x` on((`i`.`an` = `x`.`an`))) where (`i`.`dchdate` between cast((now() - interval 1 month) as date) and cast(now() as date)) group by `i`.`an` ;

-- ----------------------------
-- View structure for `list_ipd`
-- ----------------------------
DROP VIEW IF EXISTS `list_ipd`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `list_ipd` AS select `i`.`an` AS `an`,`o`.`hn` AS `hn`,`p`.`pop_id` AS `cid`,if((`p`.`pname` = ''),cast((case `p`.`male` when 1 then if((`p`.`mrtlst` < 6),if((timestampdiff(YEAR,`p`.`brthdate`,now()) < 15),'ด.ช.','นาย'),if((timestampdiff(YEAR,`p`.`brthdate`,now()) < 20),'เณร','พระ')) when 2 then if((`p`.`mrtlst` = 1),if((timestampdiff(YEAR,`p`.`brthdate`,now()) < 15),'ด.ญ.','น.ส.'),if((`p`.`mrtlst` < 6),'นาง','แม่ชี')) end) as char(8) charset utf8),convert(`p`.`pname` using utf8)) AS `prename`,concat(`p`.`fname`,' ',`p`.`lname`) AS `ptname`,timestampdiff(YEAR,`p`.`brthdate`,`o`.`vstdttm`) AS `age`,`cc`.`symptom` AS `cc`,group_concat(distinct `pi`.`pillness` order by `pi`.`id` ASC separator '') AS `pi`,group_concat(distinct `ph`.`phistory` order by `ph`.`id` ASC separator '\r\n') AS `ph`,group_concat(distinct `pe`.`sign` order by `pe`.`id` ASC separator '\r\n') AS `pe`,`o`.`bw` AS `bw`,`o`.`tt` AS `tt`,`o`.`sbp` AS `sbp`,`o`.`dbp` AS `dbp`,`o`.`pr` AS `pr`,`o`.`rr` AS `rr`,`o`.`height` AS `height`,`o`.`bmi` AS `bmi`,group_concat(distinct `x`.`icd10name` separator ',') AS `prediag`,concat(`lp`.`prename`,`d`.`fname`,' ',`d`.`lname`) AS `doctor`,`d`.`lcno` AS `lcno` from (((((((((`ipt` `i` join `pt` `p` on((`i`.`hn` = `p`.`hn`))) join `ovst` `o` on((`i`.`vn` = `o`.`vn`))) join `symptm` `cc` on((`o`.`vn` = `cc`.`vn`))) left join `pillness` `pi` on((`o`.`vn` = `pi`.`vn`))) left join `phistory` `ph` on((`o`.`vn` = `ph`.`vn`))) left join `sign` `pe` on((`o`.`vn` = `pe`.`vn`))) left join `ovstdx` `x` on(((`o`.`vn` = `x`.`vn`) and (`x`.`cnt` = 1)))) left join `dct` `d` on((`o`.`dct` = `d`.`lcno`))) left join `l_prename` `lp` on((`d`.`pname` = `lp`.`prename_code`))) where (`i`.`rgtdate` between cast((now() - interval 1 month) as date) and cast(now() as date)) group by `i`.`an` ;

-- ----------------------------
-- View structure for `pe`
-- ----------------------------
DROP VIEW IF EXISTS `pe`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `pe` AS select `sign`.`vn` AS `vn`,group_concat(`sign`.`sign` order by `sign`.`id` ASC separator ' ') AS `pe` from `sign` group by `sign`.`vn` ;

-- ----------------------------
-- View structure for `ph`
-- ----------------------------
DROP VIEW IF EXISTS `ph`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `ph` AS select `phistory`.`vn` AS `vn`,group_concat(`phistory`.`phistory` order by `phistory`.`id` ASC separator ',') AS `ph` from `phistory` group by `phistory`.`vn` ;

-- ----------------------------
-- View structure for `pi`
-- ----------------------------
DROP VIEW IF EXISTS `pi`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `pi` AS select `pillness`.`vn` AS `vn`,group_concat(`pillness`.`pillness` order by `pillness`.`id` ASC separator '') AS `pi` from `pillness` group by `pillness`.`vn` ;

-- ----------------------------
-- View structure for `pt_allergy`
-- ----------------------------
DROP VIEW IF EXISTS `pt_allergy`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `pt_allergy` AS select `a`.`hn` AS `hn`,group_concat(distinct substring_index(`a`.`namedrug`,' ',1) separator ',') AS `drug` from `allergy` `a` group by `a`.`hn` ;

-- ----------------------------
-- View structure for `ptinfo`
-- ----------------------------
DROP VIEW IF EXISTS `ptinfo`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `ptinfo` AS select `p`.`hn` AS `hn`,timestampdiff(YEAR,`p`.`brthdate`,now()) AS `age`,if((`p`.`pname` = ''),cast((case `p`.`male` when 1 then if((`p`.`mrtlst` < 6),if((timestampdiff(YEAR,`p`.`brthdate`,now()) < 15),'ด.ช.','นาย'),if((timestampdiff(YEAR,`p`.`brthdate`,now()) < 20),'เณร','พระ')) when 2 then if((`p`.`mrtlst` = 1),if((timestampdiff(YEAR,`p`.`brthdate`,now()) < 15),'ด.ญ.','น.ส.'),if((`p`.`mrtlst` < 6),'นาง','แม่ชี')) end) as char(8) charset utf8),convert(`p`.`pname` using utf8)) AS `prename`,concat(`p`.`fname`,' ',`p`.`lname`) AS `ptname`,`p`.`pop_id` AS `cid`,`p`.`brthdate` AS `birth`,`s`.`namemale` AS `sex`,concat(`p`.`addrpart`,' ',`m`.`namemoob`,' ',`b`.`nametumb`,' ',`a`.`nameampur`,' ',`c`.`namechw`) AS `address` from (((((`pt` `p` join `male` `s` on((`p`.`male` = `s`.`male`))) left join `changwat` `c` on((`p`.`chwpart` = `c`.`chwpart`))) left join `ampur` `a` on(((`p`.`chwpart` = `a`.`chwpart`) and (`p`.`amppart` = `a`.`amppart`)))) left join `tumbon` `b` on(((`p`.`chwpart` = `b`.`chwpart`) and (`p`.`amppart` = `b`.`amppart`) and (`p`.`tmbpart` = `b`.`tmbpart`)))) left join `mooban` `m` on(((`p`.`chwpart` = `m`.`chwpart`) and (`p`.`amppart` = `m`.`amppart`) and (`p`.`tmbpart` = `m`.`tmbpart`) and (`p`.`moopart` = `m`.`moopart`)))) ;

-- ----------------------------
-- Auto increment value for `doctor_orders`
-- ----------------------------
ALTER TABLE `doctor_orders` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `nurse_notes`
-- ----------------------------
ALTER TABLE `nurse_notes` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `user`
-- ----------------------------
ALTER TABLE `user` AUTO_INCREMENT=8;
ALTER TABLE `tpr`
ADD COLUMN `oral_fluid`  int NULL AFTER `user`,
ADD COLUMN `parenteral`  int NULL AFTER `oral_fluid`,
ADD COLUMN `urine`  int NULL AFTER `parenteral`,
ADD COLUMN `emesis`  int NULL AFTER `urine`,
ADD COLUMN `drainage`  int NULL AFTER `emesis`,
ADD COLUMN `aspiration`  int NULL AFTER `drainage`,
ADD COLUMN `stools`  int NULL AFTER `aspiration`,
ADD COLUMN `urines`  int NULL AFTER `stools`;

