<?php

namespace app\controllers;

use app\models\HiSetup;
use app\models\Idpm;
use app\models\Ipt;
use app\models\Iptadm;
use app\models\IptSearch;
use app\models\Lab;
use app\models\Lbbk;
use app\models\ListIpd;
use app\models\Ovst;
use app\models\Pt;
use app\models\PtAllergy;
use app\models\Ptinfo;
use kartik\mpdf\Pdf;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

date_default_timezone_set('Asia/Bangkok');

/**
 * AdmissionController implements the CRUD actions for Ipt model.
 */
class AdmissionController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ]
                    ]
                ]
            ]
        );
    }

    /**
     * Lists all Ipt models.
     *
     * @return string
     */
    public function actionIndex($ward_id)
    {
        $searchModel = new IptSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        $dataProvider->query->andWhere(['ward' => $ward_id]);
        $dataProvider->query->andWhere(['dchdate' => '0000-00-00']);
        $ward = Idpm::find()->where(['idpm' => $ward_id])->one();
        $model = $dataProvider->getModels();
        $layout = HiSetup::find()->where(['common_code'=>'layout'])->one();
        if ($layout->value1 == 'grid'){
            return $this->render('index_grid', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'ward' => $ward,
                'layout' => $layout->value1,
            ]);    
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
                'ward' => $ward,
                'layout' => $layout->value1,
            ]);    
        }
    }

    /**
     * Displays a single Ipt model.
     * @param int $an An
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($an)
    {
        $model = ListIpd::find()->where(['an'=>$an])->one();
        $ptinfo = Ptinfo::find()->where(['hn'=>$model->hn])->one();
        $allergy = PtAllergy::find()->where(['hn'=>$model->hn])->one();
        $visit = Ovst::find()->where(['an' => $an])->one();
        $ward = Ipt::find()->where(['an' => $an])->one();

        return $this->render('view', [
            'model' => $model,
            'ptinfo' => $ptinfo,
            'allergy' => $allergy,
            'visit' => $visit,
            'ward' => $ward,
        ]);
    }

    /**
     * Displays a single Ipt model.
     * @param int $an An
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionLab($id)
    {
        $model = ListIpd::find()->where(['an'=>$id])->one();
        $ptinfo = Ptinfo::find()->where(['hn'=>$model->hn])->one();
        $visit = Ovst::find()->where(['an' => $id])->one();
        $ward = Ipt::find()->where(['an' => $id])->one();

        return $this->render('lab', [
            'model' => $model,
            'ptinfo' => $ptinfo,
            'visit' => $visit,
            'ward' => $ward,
        ]);
    }
    /**
     * Creates a new Ipt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Ipt();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'an' => $model->an]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ipt model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $an An
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($an)
    {
        $model = $this->findModel($an);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'an' => $model->an]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionSwitchlayout($id)
    {
        $layout = HiSetup::find()->where(['common_code'=>'layout'])->one();
        if ($layout->value1 == 'grid'){
            $layout->value1 = 'card';
            $layout->save();
        } else {
            $layout->value1 = 'grid';
            $layout->save();
        }
        return $this->redirect(['index','ward_id' => $id]);

    }

    public function actionLayout()
    {
        $layout = HiSetup::find()->where(['common_code'=>'layout'])->one();
        if ($layout->value2 == 'grid'){
            $layout->value2 = 'card';
            $layout->save();
        } else {
            $layout->value2 = 'grid';
            $layout->save();
        }
        return $this->redirect(['user/admin/index']);

    }

    public function actionPrintLab($vn,$labcode)
    {
        $model = Lbbk::find()->where(['vn'=>$vn,'labcode'=>$labcode])->all();
        $lab = Lab::find()->where(['labcode' => $labcode])->one();
        $pt = Ipt::find()->where(['vn' => $vn])->one();
        $content = $this->renderPartial('_print',[
            'model' => $model,
            'pt' => $pt,
            'lab' => $lab, 
        ]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Lab : '.$pt->an],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    /**
     * Deletes an existing Ipt model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $an An
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($an)
    {
        $this->findModel($an)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ipt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $an An
     * @return Ipt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($an)
    {
        if (($model = Ipt::findOne(['an' => $an])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
